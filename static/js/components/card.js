Vue.component('Card', {
  props: ['icon', 'title', 'link'],
  template: `
    <a class="card-link" :href="link">
      <div class="card">
        <div class="card-icon" :class="'card-icon_' + title.toLowerCase()"><i :class="icon"></i></div>
        <div class="card-body">
          <h3>{{ title }}</h3>
          <slot></slot>
        </div>
      </div>
    </a>
  `,
});
