readInSidebarState = () => {
  if (document.cookie.includes("sidebarCollapsed=true")) {
    return false;
  }
  return true;
}

Vue.component('Sidebar', {
  data: () => ({
    expanded: readInSidebarState(),
  }),
  props: ['forcecollapse'],
  template: `
  <div
    class="sidebar-container"
    v-bind:class="{ expanded: getExpandState() }">
    <nav class="sidebar">
      <ul class="nav">
        <slot></slot>
        <sidebar-item
          icon="fa-angle-double-left"
          title="Close"
          v-if="getExpandState() && !forcecollapse"
          v-on:expandSidebar="handleExpandSidebar"
        ></sidebar-item>
        <sidebar-item
          icon="fa-angle-double-right"
          title="Expand"
          v-if="!getExpandState() && !forcecollapse"
          v-on:expandSidebar="handleExpandSidebar"
        ></sidebar-item>
      </ul>
    </nav>
  </div>
`,
methods: {
  getExpandState() {
    if (this.forcecollapse) {
      return false;
    }
    return this.expanded;
  },

  handleExpandSidebar() {
    this.expanded = !this.expanded;
    // Set a cookie if they collpase the sidebar
    if (this.expanded) {
      document.cookie = "sidebarCollapsed=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    } else {
      document.cookie = "sidebarCollapsed=true; path=/;";
    }
  },
}});
