Vue.component('PageHeader', {
  props: ['title', 'icon'],
  template: `
  <div class="jumbotron pageheader">
    <h2>
      <i
        class="fa"
        :class="icon"
        :title="title"
        v-if="icon"
      ></i>
      {{ title }}
    </h2>
    <div class="pageheader-content">
      <slot></slot>
    </div>
  </div>
`,
});
