Vue.component('InfoPanel', {
  props: ['title'],
  template: `
  <div class="info-panel panel panel-default">
    <div class="panel-heading">
      <i class="fa fa-info-circle"></i> 
      <h3 class="panel-title">{{ title }}</h3>
    </div>
    <div class="panel-body">
      <slot></slot>
    </div>
  </div>
`,
});
