Vue.component('SidebarItem', {
  props: ['active', 'icon', 'link', 'title'],
  template: `
    <li
      class="sidebar-item"
      :class="'sidebar-section-' + title.toLowerCase()">
      <a
        :href="link"
        :class="active ? 'active' : ''"
        v-on:click="handleClick">
        <i
          class="fa sidebar-item_icon"
          :class="icon"
          :title="title"
        ></i>
        <p class="sidebar-title">{{ title }}</p>
      </a>
    </li>
`,
methods: {
  handleClick() {
    if (this.title === "Expand" || this.title === "Close") {
      this.$emit('expandSidebar');
    }
  },
}});
