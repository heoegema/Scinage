from layers.models import GroupMembership, ContentGroup
from django.shortcuts import get_object_or_404


def get_item_permissions(user, item):
    perms = {'own': False, 'share': False, 'edit': False, 'delete': False, 'use': False}
    itemtype = item.__class__.__name__
    # First we shall see if the user owns the item...
    if item is None or item.owner == user:
        perms = dict.fromkeys(perms, True)
        return perms
    # Next see if the user owns any groups this item is in...
    owned = ContentGroup.objects.filter(**{"%ss__in" % itemtype.lower(): [item], 'owner': user}).count()
    if owned:
        perms.update({'edit': True, 'use': True})
        return perms
    # Then we shall see if the item has any memberships affiliated with this user
    memberships = GroupMembership.objects.filter(**{"contentgroup__%ss__in" % itemtype.lower(): [item], 'user': user, 'contentgroup__in': item.contentgroup_set.all()})
    if memberships.filter(can_edit_items=True).count():
        perms.update({'edit': True})
    if memberships.filter(can_use_items=True).count():
        perms.update({'use': True})
    if itemtype == 'Layer' and item.shared:
        perms.update({'use': True})
    return perms

def group_perms(user, group_id=None):
    if group_id != None:
        perms = {
            'can_add_users': False,
            'can_remove_users': False,
            'can_use_items': False,
            'can_add_items': False,
            'can_remove_items': False,
            'can_edit_items': False,
            'can_edit_permissions': False
        }
        group = get_object_or_404(ContentGroup, pk=group_id)
        if group.owner == user or user.is_superuser:
            perms = dict.fromkeys(perms, True)
        else:
            groupmembership = GroupMembership.objects.filter(contentgroup=group, user=user).first()
            if groupmembership:
                perms = {
                    'can_add_users': groupmembership.can_add_users,
                    'can_remove_users': groupmembership.can_remove_users,
                    'can_use_items': groupmembership.can_use_items,
                    'can_add_items': groupmembership.can_add_items,
                    'can_remove_items': groupmembership.can_remove_items,
                    'can_edit_items': groupmembership.can_edit_items,
                    'can_edit_permissions': groupmembership.can_edit_permissions,
                }
        return perms
