import json
from .models import *

from django.shortcuts import render
from django.views.decorators.vary import vary_on_cookie
from django.db.models import Count, DateTimeField
from django.db.models.functions import TruncMonth, TruncWeek

from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.models import User

from datetime import datetime, timedelta

# number of users
@staff_member_required
def user_metrics(request):

    users = User.objects \
        .annotate(month=TruncMonth('date_joined')) \
        .values('month') \
        .annotate(id_count=Count('id')) \
        .order_by('month')

    user_signup_data = list()
    for user in users:
        entry = [int(datetime.timestamp(user['month'])*1000), user['id_count']]
        user_signup_data.append(entry)

    total_user_data = list()
    acc_user_count = 0
    for data in user_signup_data:
        entry = [data[0], data[1] + acc_user_count]
        acc_user_count += data[1]
        total_user_data.append(entry)

    return total_user_data, user_signup_data

# widgets, slides and slideshows created
def query(obj):
    return obj.objects \
        .annotate(week=TruncWeek('creation_date')) \
        .values('week') \
        .annotate(id_count=Count('id')) \
        .order_by()

def preprocess(input):
    output = list()
    for data in input:
        entry = [int(datetime.timestamp(data['week'])*1000), data['id_count']]
        output.append(entry)
    return output

@staff_member_required
def creation_metrics(request):

    layers = query(Layer)
    slides = query(Slide)
    slideshows = query(Slideshow)

    data_layers = preprocess(layers)
    data_slides = preprocess(slides)
    data_slideshows = preprocess(slideshows)

    return data_layers, data_slides, data_slideshows

#pie chart of owner id and screen connect
@staff_member_required
def screen_pie_metrics(request):

    screens_online = Screen.objects \
        .filter(last_connect__gte = datetime.now()-timedelta(minutes=0.5))

    users = screens_online \
        .values('owner_id') \
        .annotate(id_count=Count('owner_id')) \
        .order_by('-id_count')[:10]

    total = 0
    for iter in range(0, len(users)):
        users[iter]['username'] = str(User.objects.get(pk=users[iter]['owner_id']))

        users[iter]['screen_info'] = ""
        for screen_info in screens_online.filter(owner_id=users[iter]['owner_id']).values('ip','title'):
            users[iter]['screen_info'] += screen_info['title']+'-'+screen_info['ip']+', '
        users[iter]['screen_info'] = users[iter]['screen_info'][:-2]

        total += users[iter]['id_count']

    return users, total

#graph showing number of screens pinged at certain dates
@staff_member_required
def screen_metrics(request):

    users = Screen.objects \
        .annotate(week=TruncWeek('last_connect')) \
        .values('week') \
        .annotate(id_count=Count('id')) \
        .order_by()

    screens = list()
    for user in users:
        if user['week'] is not None:
            entry = [int(datetime.timestamp(user['week'])*1000), user['id_count']]
            screens.append(entry)

    return screens

#graph showing most-used x widgets
@staff_member_required
def widget_metrics(request):

    widgets = list()
    for layer in Layer.objects.all():
        uses_count = Layer.uses(Layer.objects.get(pk=layer.id))
        widgets.append({'count':uses_count, 'title':layer.title})

    widgets = sorted(widgets, key=lambda x: x['count'], reverse=True)[:min(15,len(widgets))]

    # widgets = SlideLayer.objects \
    #     .values('title') \
    #     .annotate(count=Count('slide_id', distinct=True)) \
    #     .order_by('-count')[:10]

    return widgets

@staff_member_required
@vary_on_cookie
def dashboard_all(request):
    total_user_data, user_signup_data = user_metrics(request)
    data_layers, data_slides, data_slideshows = creation_metrics(request)
    screens_pie, total = screen_pie_metrics(request)
    screens = screen_metrics(request)
    widgets = widget_metrics(request)

    return render(request, 'dashboard/dashboard.html',
        {'total_users': json.dumps(total_user_data), 'user_signup': json.dumps(user_signup_data),
        'layers': json.dumps(data_layers), 'slides': json.dumps(data_slides), 'slideshows': json.dumps(data_slideshows),
        'screens_pie': screens_pie, 'total': total, 'screens': json.dumps(screens), 'widgets': widgets})
