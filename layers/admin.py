from django.contrib import admin
from .models import *

class LayerAdmin(admin.ModelAdmin):
	list_display = ['title', 'owner', 'shared', 'official', 'description']
	list_filter = ['owner', 'shared', 'official']
	search_fields = ['title', 'description']

class LayerRatingAdmin(admin.ModelAdmin):
	list_display = ['get_title', 'user', 'creation_date', 'edit_date', 'rating']
	list_filter = ['rating', 'user', 'layer__title']
	search_fields = ['layer__title', 'creation_date', 'edit_date']

	def get_title(self, obj):
		return obj.layer.title
	get_title.admin_order_field = 'layer'
	get_title.short_description = 'Layer title'

class VersionAdmin(admin.ModelAdmin):
	list_display = ['get_title', 'creation_date', 'edit_date', 'publish', 'changes']
	list_filter = ['publish', 'layer__title']
	ordering = ['-edit_date']
	search_fields = ['layer__title', 'creation_date', 'edit_date', 'changes']

	def get_title(self, obj):
		return obj.layer.title
	get_title.admin_order_field = 'layer'
	get_title.short_description = 'Layer title'

class VersionFeedbackAdmin(admin.ModelAdmin):
	list_display = ['get_layer_title', 'get_version_id', 'user', 'creation_date', 'feedback_type', 'feedback']
	list_filter = ['feedback_type', 'user']
	ordering = ['-creation_date', 'version__layer__title', 'feedback_type']
	search_fields = ['version__layer__title', 'creation_date', 'feedback']

	def get_layer_title(self, obj):
		return obj.version.layer.title
	get_layer_title.admin_order_field = 'version__layer__title'
	get_layer_title.short_description = 'Layer title'

	def get_version_id(self, obj):
		return obj.version.id
	get_version_id.admin_order_field = 'version_id'
	get_version_id.short_description = 'Version Id'

class SlideAdmin(admin.ModelAdmin):
	list_display = ['title', 'owner', 'creation_date', 'edit_date', 'last_edit']
	list_filter = ['owner', 'last_edit']
	ordering = ['-edit_date']
	search_fields = ['title', 'creadtion_date', 'edit_date']

class SlideLayerAdmin(admin.ModelAdmin):
	list_display = ['get_slide_title', 'title', 'get_version_title', 'index']
	list_filter = ['slide__title', 'version__layer__title']
	search_fields = ['title']

	def get_slide_title(self, obj):
		return obj.slide.title
	get_slide_title.admin_order_field = 'slide'
	get_slide_title.short_description = 'Slide title'

	def get_version_title(self, obj):
		return obj.version.layer.title
	get_version_title.admin_order_field = 'version.layer'
	get_version_title.short_description = 'Version title'

class SlideshowAdmin(admin.ModelAdmin):
	list_display = ['title', 'owner', 'creation_date', 'edit_date', 'last_edit']
	list_filter = ['owner', 'last_edit']
	ordering = ['-edit_date']
	search_fields = ['title', 'creadtion_date', 'edit_date']

class SlideshowSlideAdmin(admin.ModelAdmin):
	list_display = ['get_slideshow_title', 'get_slide_title']
	list_filter = ['slideshow__title', 'slide__title']

	def get_slideshow_title(self, obj):
		return obj.slideshow.title
	get_slideshow_title.admin_order_field = 'slideshow'
	get_slideshow_title.short_description = 'Slideshow title'

	def get_slide_title(self, obj):
		return obj.slide.title
	get_slide_title.admin_order_field = 'slide'
	get_slide_title.short_description = 'Slide title'

class ScreenAdmin(admin.ModelAdmin):
	list_display = ['title', 'owner', 'slug', 'link', 'last_connect', 'description', 'ip']
	list_filter = ['owner']

class ContentGroupAdmin(admin.ModelAdmin):
	list_display = ['title', 'owner','description']
	list_filter = ['owner']

class GroupMembershipAdmin(admin.ModelAdmin):
	list_display = ['user', 'get_title', 'can_add_users', 'can_remove_users', 'can_use_items', 'can_add_items', 'can_remove_items', 'can_edit_items']
	list_filter = ['user', 'contentgroup__title']

	def get_title(self, obj):
		return obj.contentgroup.title
	get_title.admin_order_field = 'contentgroup'
	get_title.short_description = 'Group title'

class CachedDataAdmin(admin.ModelAdmin):
	list_display = ['url', 'timestamp']
	ordering = ['-timestamp']

admin.site.register(InputType)
admin.site.register(Layer, LayerAdmin)
admin.site.register(LayerRating, LayerRatingAdmin)
admin.site.register(Version, VersionAdmin)
admin.site.register(VersionFeedback, VersionFeedbackAdmin)
admin.site.register(Slide, SlideAdmin)
admin.site.register(SlideLayer, SlideLayerAdmin)
admin.site.register(Slideshow, SlideshowAdmin)
admin.site.register(SlideshowSlide, SlideshowSlideAdmin)
admin.site.register(Screen, ScreenAdmin)
admin.site.register(ContentGroup, ContentGroupAdmin)
admin.site.register(GroupMembership, GroupMembershipAdmin)
admin.site.register(CachedData, CachedDataAdmin)