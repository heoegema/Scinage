from django import template
from layers.functions import get_item_permissions, group_perms
from layers.models import FavoriteLayer, LayerRating, SlideshowSlide, Slideshow, Screen
from jinja2 import evalcontextfilter, Markup, escape

register = template.Library()


@register.inclusion_tag('layers/layer_list_fav.html', takes_context=True)
def layer_item_fav(context, layer, mode='manage'):
    return {
        'user': context['user'],
        'layer': layer,
        'mode': mode,
        'favorite': FavoriteLayer.objects.filter(user=context['user'], layer=layer).count(),
        'rating': LayerRating.objects.filter(user=context['user'], layer=layer).first(),
        'perms': get_item_permissions(context['user'], layer)
    }


@register.inclusion_tag('screens/screen_item.html', takes_context=True)
def screen_item(context, screen, domain=""):
    return {
        'user': context['user'],
        'screen': screen,
        'domain': domain,
        'perms': get_item_permissions(context['user'], screen),
    }


@register.inclusion_tag('slides/slide_list_single_new.html', takes_context=True)
def slide_item(context, slide):
    # Determine if a user is able to edit / share here
    return {
        'user': context['user'],
        'slide': slide,
        'mode': context['mode'],
        'perms': get_item_permissions(context['user'], slide)
    }


@register.inclusion_tag('groups/group_list_single.html', takes_context=True)
def group_item(context, group):
    return {
        'user': context['user'],
        'group': group,
        'perms': group_perms(context['user'], group.id),
        'mode': context['mode']
    }


@register.inclusion_tag('slideshows/slideshow_list_single_new.html', takes_context=True)
def slideshow_item(context, slideshow):
    return {
        'user': context['user'],
        'slideshow': slideshow,
        'mode': context['mode'],
        'perms': get_item_permissions(context['user'], slideshow)
    }


@register.inclusion_tag('slides/slidelayers_template.html', takes_context=True)
def slidelayers_all(context):
    return {
        'layerset': context['layerset']
    }


@register.inclusion_tag('slides/slidelayer_template.html')
def slidelayers_single(form):
    return{
        'form': form
    }


@register.inclusion_tag('slideshows/slideshowslides_template.html', takes_context=True)
def slideshowslides_all(context):
    return {
        'slideset': context['slideset']
    }


@register.inclusion_tag('slideshows/slideshowslides_single_template.html')
def slideshowslides_single(form):
    return{
        'form': form
    }

@register.inclusion_tag('groups/sharegroup_template.html')
def sharegroups_single(form):
    return{
        'form': form
    }

@register.inclusion_tag('slides/slide_slideshows.html')
def slide_item_slideshow(slide):

    slideshows = SlideshowSlide.objects.filter(slide_id=slide.id)
    slideshow_list = list()
    slideshow_ids = []
    for slideshow in slideshows:
        title = Slideshow.objects.filter(id=slideshow.slideshow_id)
        if len(title) > 0 and slideshow.slideshow_id not in slideshow_ids:
            slideshow_list.append({"title": title[0].title, "id": slideshow.slideshow_id})
            slideshow_ids.append(slideshow.slideshow_id)

    return {
        'slideshow_list': slideshow_list
    }

@register.inclusion_tag('slideshows/slideshow_screens.html')
def slideshow_item_screen(slideshow):

    slideshow_link = '/slideshows/view/'+str(slideshow.id)+'/'
    screens = Screen.objects.filter(link=slideshow_link)
    screen_list = list()
    for screen in screens:
        screen_list.append({"title": screen.title, "id": screen.id})

    return {
        'screen_list': screen_list
    }


##### Filters

# canadd does not seem to be used...
@register.simple_tag(takes_context=True)
def canadd(context, group):
    if group.owner == context['user']:
        return True
    elif group.groupmembership_set.filter(user=context['user']).first().can_add_items is True:
        return True
    else:
        return False


@register.filter(name='url_short')
def url_short(value):
    url = value.split('/')
    return url[-1]
