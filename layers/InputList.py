class ITPUT(object):
    def __init__(self, title, html, validator, validator_error, verbose_processor=None):
        self.title = title
        self.html = html
        self.validator = validator
        self.validator_error = validator_error
        self.verbose_processor = verbose_processor

    def process_verbose(self, control_verbose):
        return control_verbose if self.verbose_processor is None\
            else self.verbose_processor(control_verbose)

    def __unicode__(self):
        return self.title


InputList = {
    1: ITPUT(
        title="Text",
        html="""<div class="form-group">
                    <label>{{control_title}}</label>
                    <input
                        type="text"
                        class="form-control control-input-class"
                        name="{{control_variable}}"
                        value="{{control_value|escape}}"
                    >
                    <span class="help-block">
                        <small>{{control_verbose}}</small>
                    </span>
                </div>""",
        validator=".*",
        validator_error="Invalid text",
        ),
    2: ITPUT(
        title="Colorpicker",
        html="""<div class="form-group">
                    <label>{{control_title}}</label>
                    <input
                        type="color"
                        class="form-control control-input-class"
                        name="{{control_variable}}"
                        value="{{control_value|escape}}"
                    >
                    <span class="help-block">
                        <small>{{control_verbose}}</small>
                    </span>
                </div>""",
        validator="^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$",
        validator_error="Must be in the format #FFFFFF",
        ),
    3: ITPUT(
        title="Image",
        html="""<div class="form-group">
                    <label>{{control_title}}</label>
                    <input
                        type="text"
                        class="form-control control-input-class
                            image-select-input"
                        name="{{control_variable}}"
                        value="{{control_value|escape}}"
                    >
                    <span class="help-block">
                        <small>{{control_verbose}}</small>
                    </span>
                </div>""",
        validator=".*",
        validator_error="Must be valid URL",
        ),
    4: ITPUT(
        title="Textarea",
        html="""<div class="form-group">
                    <label>{{control_title}}</label>
                    <textarea class="form-control control-input-class"
                        name="{{control_variable}}">
                        {{control_value|escape}}
                    </textarea>
                    <span class="help-block">
                        <small>{{control_verbose}}</small>
                    </span>
                </div>""",
        validator=".*",
        validator_error="Bad Text",
        ),
    5: ITPUT(
        title="Font (Google Fonts)",
        html="""<div class="form-group">
                    <label>{{control_title}}</label>
                    <br/>
                    <input
                        type="text"
                        class="fontselect control-input-class"
                        name="{{control_variable}}"
                        value="{{control_value|escape}}"
                    >
                    <span class="help-block">
                        <small>{{control_verbose}}</small>
                    </span>
                </div>""",
        validator=".*",
        validator_error="Bad Font",
        ),
    6: ITPUT(
        title="Video",
        html="""<div class="form-group">
                    <label>{{control_title}}</label>
                    <input
                        type="text"
                        class="form-control control-input-class
                            video-select-input"
                        name="{{control_variable}}"
                        value="{{control_value|escape}}"
                    >
                    <span class="help-block">
                        <small>{{control_verbose}}</small>
                    </span>
                </div>""",
        validator=".*",
        validator_error="Must be valid URL",
        ),
    7: ITPUT(
        title="Select",
        html="""<div class="form-group">
                    <label>{{control_title}}</label>
                    <select
                        name="{{control_variable}}"
                        class="form-control control-input-class
                            select-input"
                    >
                    {% for x in control_verbose %}
                        <option {% if x == control_value|escape %}selected{% endif %} value="{{x}}">{{x}}</option>
                    {% endfor %}
                    </select>
                </div>""",
        validator=".*",
        validator_error="Select options must be comma separated in the verbose.",
        verbose_processor = lambda x: x.split(',')
        ),
}
