from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator, ValidationError, MaxValueValidator, MinValueValidator

from django.db.models.deletion import Collector
from django.db.models.fields.related import ForeignKey

from django.utils.text import slugify

from imagekit.models import ProcessedImageField, ImageSpecField
from imagekit.processors import ResizeToFit, ResizeToFill

from taggit.managers import TaggableManager
from haystack import signals

# Trashbin imports
from django.utils.translation import ugettext_lazy as _
try:
    from django.utils import timezone as datetime
except ImportError:
    from datetime import datetime

# Template Rendering with jinja
from jinja2 import Template

from .managers import *

# Input stuff
from .InputList import *


# For setting min max values on integerfields
class IntegerRangeField(models.IntegerField):
    def __init__(
        self,
        verbose_name=None,
        name=None,
        min_value=None,
        max_value=None,
        **kwargs
    ):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value': self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)


# django-undelete models fixed
class TrashableMixin(models.Model):
    trashed_at = models.DateTimeField(
        _('Trashed'),
        editable=False,
        blank=True,
        null=True
    )

    objects = NonTrashedManager()
    trash = TrashedManager()
    everything = models.Manager()

    def delete(self, *args, **kwargs):
        # keyword argument trash has default value True
        trash = kwargs.get('trash', True)
        if not self.trashed_at and trash:
            self.trashed_at = datetime.now()
            self.save()
        else:
            super(TrashableMixin, self).delete(*args, **kwargs)

    def restore(self, commit=True):
        self.trashed_at = None
        if commit:
            self.save()

    class Meta:
        abstract = True


"""
When a user creates a new layer, it will create a version to go with it.
Anytime a user wants to change what their layer looks like, it will create a
new version for the revision, and (if it is not shared) offer to delete
older versions.

A version is simply a list of input types and the jinja2(?) html code to render
the object

Input types will each contain html / css / js to add an extra field for users
to fill in.
We'll have colorpickers and shit.
"""
variable = RegexValidator(
    r'^[a-zA-Z]{1}[0-9a-zA-Z_]*$',
    'Only alphanumeric characters are allowed. Must start with a letter.',
)


class UploadedImage(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    creation_date = models.DateTimeField(auto_now_add=True)
    image = ProcessedImageField(
        upload_to='uploaded_images',
        processors=[ResizeToFit(1920, 1080, False)],
        format='PNG',
        options={'quality': 80}
    )
    thumbnail = ImageSpecField(
        source='image',
        processors=[ResizeToFit(100, 100)],
        format='JPEG',
        options={'quality': 60}
    )


class UploadedVideo(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    creation_date = models.DateTimeField(auto_now_add=True)
    video = models.FileField(
        upload_to='uploaded_videos',
    )
    # title = models.CharField(max_length=255, blank=True)
    # thumbnail = ImageSpecField(
    #     source='video',
    #     processors=[ResizeToFit(100, 100)],
    #     format='JPEG',
    #     options={'quality': 60}
    # )


class Layer(TrashableMixin):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    creation_date = models.DateTimeField(auto_now_add=True)
    shared = models.BooleanField(default=False)
    official = models.BooleanField(default=False)
    title = models.CharField(max_length=255)
    short = models.CharField(max_length=12)
    description = models.TextField()
    tags = TaggableManager()
    changes = models.TextField(blank=True)
    publish = models.BooleanField(default=False)
    documentation = models.TextField(blank=True)

    # tags
    icon = ProcessedImageField(
        upload_to='icons',
        processors=[ResizeToFit(72, 72)],
        format='PNG',
        options={'quality': 80},
        blank=True, null=True
    )

    def bleeding_edge(self):
        pass

    def latest(self):
        try:
            return self.version_set.filter(publish=True).order_by(
                '-creation_date'
            )[0:1].get()
        except:
            return self.version_set.order_by('-creation_date')[0:1].get()

    def uses(self):
        return SlideLayer.objects.filter(version__layer=self).count()

    def get_slides(self):
        return SlideLayer.objects.filter(version__layer=self).values("slide")

    def favs(self):
        return self.favoritelayer_set.all().count()

    @property
    def rating(self):
        return round(self.layerrating_set.aggregate(
            models.Avg('rating'))['rating__avg'],
            1
        )

    def clone(self, user):
        newlayer = Layer.objects.get(id=self.id)
        newlayer.id = None
        newlayer.public = False
        newlayer.official = False
        newlayer.title = "CLONE OF %s" % self.title
        newlayer.owner = user
        newlayer.publish = False
        newlayer.changes = 'First version.'
        newlayer.shared = False
        newlayer.save()
        for x in self.tags.all():
            newlayer.tags.add(x)
        newlayer.save()
        # And make the version...
        oldversion = self.latest()
        newversion = self.latest()
        newversion.publish = False
        newversion.id = None
        newversion.layer = newlayer
        newversion.save()
        for control in oldversion.control_set.all():
            newcontrol = Control(
                version=newversion,
                title=control.title,
                variable_name=control.variable_name,
                verbose=control.verbose,
                input_type=control.input_type,
                default=control.default
            )
            newcontrol.save()
        return newlayer


class FavoriteLayer(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    layer = models.ForeignKey(Layer, on_delete=models.CASCADE)


class LayerRating(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    layer = models.ForeignKey(Layer, on_delete=models.CASCADE)
    rating = IntegerRangeField(min_value=1, max_value=5)
    creation_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta():
        unique_together = ('user', 'layer')


class Version(models.Model):
    layer = models.ForeignKey(Layer, on_delete=models.CASCADE)
    content = models.TextField()
    content_css = models.TextField(blank=True)
    content_js = models.TextField(blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    changes = models.TextField(blank=True)
    publish = models.BooleanField(default=False)

    # every edit after a publish will be a new version!

    last_edit = models.ForeignKey(
        User,
        blank=True,
        null=True,
        related_name='version_last_edit',
        on_delete=models.CASCADE
    )

    def save(self, publish=False, *args, **kwargs):
        if self.layer.version_set.all().count() == 0:
            new_layer = True
        else:
            new_layer = False

        super(Version, self).save(*args, **kwargs)
        if publish is True and new_layer is False:
            # We're going to do this by hand because stackoverflow only has
            # answers for ancient versions of django and I spent too bloody
            # long trying to write a general duplicator and got only nonsense
            # error messages
            # Duplicate the object to make a new HEAD version.
            new = Version(
                layer=self.layer,
                content=self.content,
                content_css=self.content_css,
                content_js=self.content_js
            )
            new.save()
            # Get all the controls too
            for control in self.control_set.all():
                newcontrol = Control(
                    version=new,
                    title=control.title,
                    variable_name=control.variable_name,
                    verbose=control.verbose,
                    input_type=control.input_type,
                    default=control.default
                )
                newcontrol.save()

    def updateAvailable(self):
        return self.layer.version_set.filter(
            creation_date__gt=self.creation_date,
            publish=True
        ).order_by('-creation_date').first()

    def compileContent(self, data=None):
        from helpers.extra import extra_globals
        compiled = """
            <html>
                <head>
                    <style>
                        %s
                    </style>
                </head>
                <body>
                    %s
                    <script>
                        window.$ = $ || window.top.$
                        parent.iframeCheck()
                        $(window).on('load', function(){
                          parent.iframeCheck(true)
                        })
                    </script>
                    <script>

                        %s
                    </script>
                </body>
            </html>
        """ % (self.content_css, self.content, self.content_js)
        template = Template(compiled)
        template.globals.update(extra_globals)

        if not data:
            data = {
                control.variable_name: control.default
                for control in self.control_set.all()
            }
        try:
            output = template.render(**data)
        except Exception as e:
            output = str(e)

        return output

    @property
    def bugs(self):
        return [
            x for x in self.versionfeedback_set.all()
            if x.feedback_type == "bug"
        ]

    @property
    def generals(self):
        return [
            x for x in self.versionfeedback_set.all()
            if x.feedback_type == "general"
        ]

    @property
    def others(self):
        return [
            x for x in self.versionfeedback_set.all()
            if x.feedback_type == "other"
        ]


class VersionFeedback(models.Model):
    version = models.ForeignKey(Version, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    creation_date = models.DateTimeField(auto_now_add=True)
    feedback_type = models.CharField(
        max_length=8,
        choices=[
            ('bug', 'Bug/Issue'),
            ('general', 'General Feedback'),
            ('other', 'Other')
        ],
        default='general'
    )
    feedback = models.TextField(blank=True)

    class Meta:
        ordering = ['-creation_date']


class InputType(models.Model):
    title = models.CharField(max_length=255)
    html = models.TextField()
    validator = models.CharField(max_length=255)
    validator_error = models.CharField(max_length=255)

    def __unicode__(self):
        return self.title


INPUT_OPTIONS = (
    (id, x.title) for id, x in InputList.items()
)


class Control(models.Model):
    version = models.ForeignKey(Version, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    variable_name = models.CharField(max_length=18)
    verbose = models.TextField(blank=True)
    input_type = models.IntegerField(
        db_column="input_type_id",
        choices=INPUT_OPTIONS
    )
    default = models.CharField(max_length=255)

    def __init__(self, *args, **kwargs):
        super(Control, self).__init__(*args, **kwargs)
        self.input_obj = InputList.get(self.input_type, None)


class Slide(TrashableMixin):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    data = models.TextField(blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now=True, blank=True, null=True)

    last_edit = models.ForeignKey(
        User,
        null=True,
        related_name='slide_edit_user',
        blank=True,
        on_delete=models.CASCADE
    )

    # Advanced Settings
    aspect_ratio = models.CharField(
        max_length=8,
        default="16x9",
        validators=[RegexValidator(
            regex='^(\d+x\d+)$',
            message='Must be in the form WidthxHeight',)]
    )

    def clone(self, user):
        # Clone the original...
        newslide = Slide.objects.get(pk=self.id)
        newslide.pk = None
        newslide.owner = user
        newslide.title = 'CLONE OF %s' % self.title
        newslide.save()

        # And get all the slidelayers
        for sl in self.slidelayer_set.all():
            sl.pk = None
            sl.slide = newslide
            sl.save()
        return newslide

    def uses(self):
        return SlideshowSlide.objects.filter(slide=self).count()

    def get_slideshows(self):
        return SlideshowSlide.objects.filter(slide=self).values("slideshow")


class SlideLayer(models.Model):
    slide = models.ForeignKey(Slide, on_delete=models.CASCADE)
    # layer = models.ForeignKey(Layer)
    x = models.FloatField(default=40)
    y = models.FloatField(default=40)
    width = models.FloatField(default=20)
    height = models.FloatField(default=20)
    title = models.CharField(max_length=100)
    index = models.IntegerField()
    version = models.ForeignKey(Version, on_delete=models.CASCADE, blank=True, null=True)
    data = models.TextField(blank=True)

    def control(self):
        return self.version.control


class Slideshow(TrashableMixin):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    overlay = models.ForeignKey(Slide, on_delete=models.CASCADE, null=True, blank=True)

    last_edit = models.ForeignKey(
        User,
        null=True,
        related_name='slideshow_last_edit',
        blank=True,
        on_delete=models.CASCADE
    )

    def clone(self, user):
        # Clone the original...
        newslideshow = Slideshow.objects.get(pk=self.id)
        newslideshow.pk = None
        newslideshow.owner = user
        newslideshow.title = 'CLONE OF %s' % self.title
        newslideshow.save()

        # And get all the slideshowslides
        for sl in self.slideshowslide_set.all():
            sl.pk = None
            sl.slideshow = newslideshow
            sl.save()
        return newslideshow


class SlideshowSlide(models.Model):
    slideshow = models.ForeignKey(Slideshow, on_delete=models.CASCADE)
    slide = models.ForeignKey(Slide, on_delete=models.CASCADE)
    length = models.IntegerField(default=15)
    order = models.IntegerField()
    time_start = models.DateTimeField(blank=True, null=True)
    time_end = models.DateTimeField(blank=True, null=True)
    end_removal = models.BooleanField(default=False)


class Screen(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(
        max_length=255,
        unique=True,
        help_text="If title changes after initial configuration, URL will change."
    )
    description = models.TextField(blank=True)
    slug = models.SlugField(max_length=255)

    link = models.CharField(max_length=255)
    ip = models.GenericIPAddressField(blank=True, null=True)
    last_connect = models.DateTimeField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.slug == '':
            potential_slug = slugify(self.title)
            num_existing = Screen.objects.filter(
                slug__startswith=potential_slug
            ).count()
            if num_existing > 0:
                potential_slug = potential_slug + '-{}'.format(
                    num_existing + 1
                )
            self.slug = potential_slug
        super(Screen, self).save(*args, **kwargs)


class ContentGroup(models.Model):
    owner = models.ForeignKey(User, related_name='owned_groups', on_delete=models.CASCADE)
    members = models.ManyToManyField(User, through='GroupMembership')
    title = models.CharField(max_length=255)
    description = models.TextField(max_length=255)

    layers = models.ManyToManyField(Layer, blank=True)
    slides = models.ManyToManyField(Slide, blank=True)
    slideshows = models.ManyToManyField(Slideshow, blank=True)
    screens = models.ManyToManyField(Screen, blank=True)

    class Meta:
        ordering = ['title']


class GroupMembership(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    contentgroup = models.ForeignKey(ContentGroup, on_delete=models.CASCADE)

    # Permissions
    can_add_users = models.BooleanField(default=False)
    can_remove_users = models.BooleanField(default=False)
    can_use_items = models.BooleanField(default=False)
    can_add_items = models.BooleanField(default=False)
    can_remove_items = models.BooleanField(default=False)
    can_edit_items = models.BooleanField(default=False)
    can_edit_permissions = models.BooleanField(default=False)


class CachedData(models.Model):
    content = models.TextField(max_length=500000)
    timestamp = models.DateTimeField(auto_now=True)
    url = models.CharField(max_length=255)


# SIGNALS
class LayerSignalProcessor(signals.BaseSignalProcessor):
    def setup(self):
        models.signals.post_save.connect(self.handle_save, sender=Layer)
        models.signals.post_delete.connect(self.handle_delete, sender=Layer)

    def teardown(self):
        models.signals.post_save.disconnect(self.handle_save, sender=Layer)
        models.signals.post_delete.disconnect(self.handle_delete, sender=Layer)
