from rest_framework import serializers, viewsets
from .forms import *
from django.http import HttpResponse, JsonResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.exceptions import PermissionDenied
from rest_framework.fields import Field
from rest_framework.filters import OrderingFilter, SearchFilter, BaseFilterBackend
from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from django.db.models import Q
from .models import *
from django.apps import apps
from django.shortcuts import HttpResponseRedirect
import decimal
from json import JSONEncoder
from uuid import UUID
import json
from django.contrib.auth.models import User

class LayerSerializer(serializers.ModelSerializer):
	class Meta:
		model = Layer

class LayerAPIViewset(viewsets.ModelViewSet):
	queryset = Layer.objects.all()
	serializer_class = LayerSerializer