from django.shortcuts import render, get_object_or_404, Http404
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect, \
    HttpRequest
from django.utils.html import escape
from django.views.decorators.clickjacking import xframe_options_exempt
from django.views.decorators.vary import vary_on_cookie
from .models import Slide, Slideshow, InputType, Version, Layer, Control,\
    SlideLayer, FavoriteLayer, UploadedImage, Screen, GroupMembership,\
    CachedData, UploadedVideo, LayerRating, VersionFeedback
from django.contrib.auth.models import User
from django.urls import reverse
from django.db.models import Count, Q, Max, F
from .forms import *
from .functions import get_item_permissions, group_perms
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.forms.models import inlineformset_factory
from jinja2 import Template
import json
from helpers.extra import extra_globals
from datetime import datetime
from django.utils import timezone
import re
from scinage.custom_logger import logger
from django.views.decorators.cache import cache_page
from django.utils.cache import get_cache_key
from django.core.cache import cache
from django.conf import settings
from django.template import loader, RequestContext
from django.contrib.auth import logout, authenticate, login, views as auth_views
from layers.forms import UserCreateForm
from django.urls import reverse_lazy
from django.views import generic
from pdf2image import convert_from_bytes
from PIL import Image
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.files.base import ContentFile
from io import StringIO
import os

try:
    from scinage.emergency import queryEmergency
except:
    from scinage.emergency_example import queryEmergency

# Create your views here.
class SignUp(generic.CreateView):
    form_class = UserCreateForm
    success_url = reverse_lazy('home')
    template_name = 'registration/signup.html'

def handler404(request, exception):
    response = render(request, 'UI/404.html',)
    response.status_code = 404
    return response


@login_required
@vary_on_cookie
def frontpage(request):
    if not request.user.is_authenticated:
        return render(request, 'UI/frontpage-nologin.html')
    else:
        username = request.user.username
        # Get all this person's shit
        owned_groups = request.user.owned_groups.all()
        other_groups = request.user.contentgroup_set.all()
        all_groups = owned_groups | other_groups
        all_groups = all_groups.distinct()

        owned_layers = request.user.layer_set.all()
        shared_layers = owned_layers.filter(shared=True)
        layer_uses = SlideLayer.objects.filter(
            version__layer__in=shared_layers
        ).count()
        other_layers = sum([
            x.layers.all().exclude(owner=request.user).count()
            for x in all_groups
        ])
        fav_layers = request.user.favoritelayer_set.all().count()

        owned_slides = request.user.slide_set.all().count()
        other_slides = sum([
            x.slides.all().exclude(owner=request.user).count()
            for x in all_groups
        ])

        owned_slideshows = request.user.slideshow_set.all().count()
        other_slideshows = sum([
            x.slideshows.all().exclude(owner=request.user).count()
            for x in all_groups
        ])

        owned_screens = request.user.screen_set.all().count()
        other_screens = sum([
            x.screens.all().exclude(owner=request.user).count()
            for x in all_groups
        ])
        screens = Screen.objects.filter(
            Q(owner=request.user) | Q(contentgroup__in=all_groups)
        ).distinct().order_by('title')
        off_screens = []
        for s in screens:
            if s.last_connect:
                time_off = datetime.now(timezone.utc) - s.last_connect
                if divmod(time_off.total_seconds(), 60)[0] > 5:
                    off_screens.append(s)
        print(off_screens)
        changed_layers = Version.objects.filter(
            layer__shared=True,
            publish=True
        ).distinct().order_by('-edit_date')[:5]

        context = {
            'username': username,
            'owned_groups': owned_groups,
            'other_groups': other_groups,
            'owned_layers': owned_layers.count(),
            'other_layers': other_layers,
            'fav_layers': fav_layers,
            'shared_layers': shared_layers.count(),
            'layer_uses': layer_uses,
            'owned_slides': owned_slides,
            'other_slides': other_slides,
            'owned_slideshows': owned_slideshows,
            'other_slideshows': other_slideshows,
            'owned_screens': owned_screens,
            'other_screens': other_screens,
            'changed_layers': changed_layers,
            'off_screens': off_screens,
        }
        return render(request, 'UI/frontpage.html', context)

# This screen shows all the widget changes
@login_required
def changeLog(request):

    # Loading changed layers
    changed_layers = Version.objects.filter(
        layer__shared=True,
        publish=True
    ).distinct().order_by('-edit_date')

    context = {
        'changed_layers': changed_layers,
    }
    
    return render(request, 'layers/layer_new_and_updated.html', context)

def ajaximage(request):
    if request.method == "POST":
        try:
            image = request.FILES.get('image')
            uim = UploadedImage.objects.create(image=image, owner=request.user)
            uim.save()
            info = {'thumb': uim.thumbnail.url, 'image': uim.image.url}
            return JsonResponse(info, safe=False)
        except Exception as e:
            return JsonResponse(
                "Failed to upload image (%s)" % str(e),
                safe=False
            )
    return JsonResponse("Not Post", safe=False)


def ajaxvideo(request):
    if request.method == "POST":
        try:
            video = request.FILES.get('video')
            uvm = UploadedVideo.objects.create(video=video, owner=request.user)
            uvm.save()
            info = {'thumb': '', 'video': uvm.video.url}
            return JsonResponse(info, safe=False)
        except Exception as e:
            return JsonResponse(
                "Failed to upload video (%s)" % str(e),
                safe=False,
            )
    return JsonResponse("Not Post", safe=False)


# Uses AJAX to add or remove layers from a user's favorites
@login_required
def toggleLayerFav(request):
    if request.method != 'POST':
        raise Http404
    layer = get_object_or_404(Layer, id=int(request.POST['id']))
    fav = FavoriteLayer.objects.filter(user=request.user, layer=layer).first()
    if fav is None:
        FavoriteLayer.objects.create(user=request.user, layer=layer)
        return HttpResponse('ADDED')
    else:
        fav.delete()
        return HttpResponse('REMOVED')


# Uses AJAX to add or remove layers from a offical IF SUPER
@login_required
def toggleLayerOff(request):
    if request.method != 'POST' or request.user.is_superuser != True:
        raise Http404
    layer = get_object_or_404(Layer, id=int(request.POST['id']))
    if layer.official is False:
        layer.official = True
        layer.save()
        return HttpResponse('ADDED')
    else:
        layer.official = False
        layer.save()
        return HttpResponse('REMOVED')


# Uses AJAX to set layer rating for a user
@login_required
def layerRating(request):
    if request.method != 'POST':
        raise Http404
    layer = get_object_or_404(Layer, id=int(request.POST['id']))
    rating = request.POST['rating']
    ratingObj = LayerRating.objects.filter(
        user=request.user,
        layer=layer
    ).first()
    if ratingObj is None:
        ratingObj = LayerRating.objects.create(
            user=request.user,
            layer=layer,
            rating=rating
        )
        return JsonResponse({'CREATED': str(layer.rating)}, safe=False)
    else:
        ratingObj.rating = rating
        ratingObj.save()
        return JsonResponse({'RATED': str(layer.rating)}, safe=False)


@login_required
def layerDocumentation(request, version_id=None):
    if not request.user.is_authenticated:
        raise Http404
    version = get_object_or_404(
        Version,
        id=version_id
    ) if version_id is not None else None
    layer = get_object_or_404(
        Layer,
        id=version.layer.id
    ) if version is not None else None
    context = {
        'layer': layer,
        'documentation': layer.documentation if layer is not None else None
    }
    return render(request, 'layers/layer_documentation.html', context)


@login_required
def versionFeedback(request, version_id=None):
    if not request.user.is_authenticated:
        raise Http404
    version = get_object_or_404(
        Version,
        id=version_id
    ) if version_id is not None else None
    versionFeedbackForm = VersionFeedbackForm()
    layer = get_object_or_404(Layer, id=version.layer.id) if version else None

    if request.method == 'POST':
        versionFeedbackForm = VersionFeedbackForm(data=request.POST)
        if versionFeedbackForm.is_valid():
            vFF = versionFeedbackForm.save(commit=False)
            if vFF.feedback:
                vFF.version = version
                vFF.user = request.user
                vFF.save()
            return HttpResponse('SUBMITTED')

    context = {
        'version': version,
        'versionFeedbackForm': versionFeedbackForm,
        'layer': layer
    }

    return render(request, 'layers/version_feedback.html', context)


@login_required
def controlsExtra(request):
    if not request.user.is_authenticated:
        raise Http404

    context = {
        'images': request.user.uploadedimage_set.order_by('-creation_date'),
        'videos': request.user.uploadedvideo_set.order_by('-creation_date'),
    }
    return render(request, 'layers/extra_control_selectors.html', context)


@login_required
def cloneLayer(request, layer_id):
    layer = get_object_or_404(Layer, pk=layer_id)
    # NEED TO VERIFY USER HAS VIEW PERMISSION!
    newlayer = layer.clone(request.user)
    return HttpResponseRedirect('/widgets/edit/%s/' % newlayer.id)


@login_required
@vary_on_cookie
def manageLayers(request):
    # for now get all official layers and all user layers
    return render(request, 'layers/layers.html')


@login_required
@vary_on_cookie
def listLayers(request, mode):
    layers = Layer.objects.filter(
        owner=request.user
    ).annotate(
        latest_version=Max('version__edit_date')
    ).order_by(F('latest_version')).reverse()
    groups = request.user.contentgroup_set.all() \
        | request.user.owned_groups.all()
    groups = groups.distinct().annotate(c=Count('layers')).filter(c__gt=0)
    # This isn't going to work if people edit eachother's stuff...
    # Also wayyy too slow
    popular = Layer.objects.filter(
        shared=True
    ).annotate(
        num_fav=Count('favoritelayer')
    ).order_by('-num_fav')[:5]
    # popular = Layer.objects.filter(shared=True)
    recent = list(set([
        x.version.layer
        for x in SlideLayer.objects.filter(
            version__layer__owner=request.user
        ).reverse()
    ]))[:4]
    favorites = set([
        x.layer for x in FavoriteLayer.objects.filter(user=request.user)
    ])
    officials = Layer.objects.filter(official=True)
    context = {
        'popular': popular,
        'layers': layers,
        'mode': mode,
        'groups': groups,
        'recent': recent,
        'favorites': favorites,
        'officials': officials,
    }
    return render(request, 'layers/layer_list.html', context)


# Create or Edit Layers.
# This is OLD AND CAN BE REMOVED...probably
@login_required
@vary_on_cookie
def editLayer(request, layer_id=None):
    prev = request.GET.get('preview', False)
    savestatus = None
    layer = None if layer_id is None else get_object_or_404(Layer, pk=layer_id)

    # Verify the user can edit this
    if layer is not None:
        if not get_item_permissions(request.user, layer).get('edit', False):
            raise Http404

    version = None if layer is None else layer.version_set.order_by(
        '-creation_date'
    )[0:1].get()
    layerform = LayerForm() if layer is None else LayerForm(instance=layer)
    formset = inlineformset_factory(
        Version,
        Control,
        form=ControlForm,
        can_delete=True,
        extra=0
    )
    thisformset = formset(prefix="controls") if layer is None else formset(
        prefix="controls",
        instance=version
    )
    versionform = VersionForm() if layer is None else VersionForm(
        instance=version
    )

    if request.method == 'POST':

        thisformset = formset(data=request.POST, prefix="controls") if layer is None else formset(instance=version, data=request.POST, prefix='controls')
        layerform = LayerForm(request.POST, request.FILES) if layer is None else LayerForm(request.POST, request.FILES, instance=layer)
        versionform = VersionForm(data=request.POST) if layer is None else VersionForm(instance=version, data=request.POST)

        if prev is not False:
            thisformset.is_valid() and versionform.is_valid()

            try:
                template = Template(versionform.cleaned_data['content'])
                template.globals.update(extra_globals)
                variables = {}
                for form in thisformset:
                    if form.cleaned_data['DELETE'] is False:
                        variables[form.cleaned_data['variable_name']] = form.cleaned_data['default']
                preview = template.render(**variables)
            except Exception as e:
                preview = "Failed to render preview. Error: %s" % str(e)
            resp = HttpResponse(preview)
            resp.__setitem__('X-XSS-Protection', '0')
            return resp

        else:
            if thisformset.is_valid() and layerform.is_valid() and versionform.is_valid():
                request.session['newlysaved'] = True
                if layer_id is None:
                    newlayer = layerform.save(commit=False)
                    if newlayer.id is None:
                        newlayer.owner = request.user
                    newlayer.save()
                    layerform.save_m2m()

                    newversion = versionform.save(commit=False)
                    newversion.layer = newlayer
                    newversion.changes = "First Version."

                    newversion.save()

                    instances = thisformset.save(commit=False)
                    for obj in thisformset.deleted_objects:
                        obj.delete()
                    for instance in instances:
                        instance.version = newversion
                        instance.save()
                    return HttpResponseRedirect('/widgets/edit/%s/' % newlayer.id)
                else:
                    layerform.save()
                    versionform.save()
                    instances = thisformset.save()
                    # for instance in instances:
                    #   print instance
                    return HttpResponseRedirect('/widgets/edit/%s/' % layer.id)
            else:
                pass
                # print versionform.errors, thisformset.errors, layerform.errors

    context = {
        'layerform': layerform,
        'versionform': versionform,
        'controlsform': thisformset,
        'helper': ControlsInlineHelper(),
        'trhelper': ControlsRowHelper(),
        'layer': layer,
        'version': version,
    }
    return render(request, 'layers/editlayer.html', context)

@login_required
@vary_on_cookie
def editLayernew(request, layer_id=None):
    prev = request.GET.get('preview', False)
    savestatus = None
    layer = None if layer_id is None else get_object_or_404(Layer, pk=layer_id)
    feedback = None
    feedback_count = 0
    # Verify the user can edit this and get feedback
    if layer is not None:
        feedback = layer.version_set.all().order_by(
            '-id'
        )[1:].prefetch_related(
            'versionfeedback_set'
        )
        for v in feedback:
            for bug in v.bugs:
                feedback_count += 1
            for general in v.generals:
                feedback_count += 1
            for other in v.others:
                feedback_count += 1
        if not get_item_permissions(request.user, layer).get('edit', False):
            raise Http404
    version = None if layer is None else layer.version_set.order_by(
        '-creation_date'
    )[0:1].get()
    layerform = LayerForm() if layer is None else LayerForm(instance=layer)
    formset = inlineformset_factory(
        Version,
        Control,
        form=ControlForm,
        can_delete=True,
        extra=0
    )
    thisformset = formset(
        prefix="controls"
    ) if layer is None else formset(prefix="controls", instance=version)
    versionform = VersionForm() if layer is None else VersionForm(
        instance=version
    )

    if version is None and layer is not None:
        content = """<html>
<head>
<!-- Link External CSS here -->
    <style>
{# BEGIN CSS #}body{
    width: 100vw;
    height: 100vh;
}{# END CSS #}
    </style>
</head>

<body>

<!-- Link External JS scripts here -->

    <script>
{# BEGIN JS #}//Put all JS here.
//It will be executed after external
//scripts are loaded.
{# END JS #}
    </script>
</body>
</html>
        """
    else:
        if version is not None:
            content = version.content
            content_css = version.content_css
            content_js = version.content_js
        else:
            content = """ """

    # Split up content... (NEED TO CLEAN THIS UP)
    # html = re.sub(r'{# BEGIN CSS #}((.|\n)*){# END CSS #}', "\t\t!!!CSS WILL GO HERE!!!", content)
    # html = re.sub(r'{# BEGIN JS #}((.|\n)*){# END JS #}', "\t\t!!!JS WILL GO HERE!!!", html)

    # css = re.findall(r'\{\# BEGIN CSS \#\}(.*?){\# END CSS \#\}',content,re.DOTALL)
    # css = "".join(css)
    # js = re.findall(r'\{\# BEGIN JS \#\}(.*?){\# END JS \#\}',content,re.DOTALL)
    # js = "".join(js)
    # layercodeform = LayerCodeForm(initial={'layerHTML': html, 'layerCSS': css, 'layerJS': js})

    if request.method == 'POST':
        thisformset = formset(
            data=request.POST,
            prefix="controls"
        ) if layer is None else formset(
            instance=version,
            data=request.POST,
            prefix='controls'
        )
        layerform = LayerForm(
            request.POST,
            request.FILES
        ) if layer is None else LayerForm(
            request.POST,
            request.FILES,
            instance=layer
        )
        versionform = VersionForm(
            data=request.POST
        ) if layer is None else VersionForm(
            instance=version,
            data=request.POST
        )
        # layercodeform = LayerCodeForm(data=request.POST)
        # layercodeform.is_valid()
        # lc = layercodeform.cleaned_data

        # content = lc.get('layerHTML', '')
        # CSS = "{# BEGIN CSS #}" + lc.get('layerCSS', '') + "{# END CSS #}"
        # JS = "{# BEGIN JS #}" + lc.get('layerJS', '') + "{# END JS #}"
        # content = re.sub(r'\!\!\!CSS WILL GO HERE\!\!\!', CSS, content)
        # content = re.sub(r'\!\!\!JS WILL GO HERE\!\!\!', JS, content)
        # versionform.data.update(content=content)

        if prev is not False:
            thisformset.is_valid() and versionform.is_valid()

            try:
                fakeversion = versionform.save(commit=False)
                variables = {}
                for form in thisformset:
                    if form.cleaned_data['DELETE'] is False:
                        variables[form.cleaned_data['variable_name']] = \
                            form.cleaned_data['default']
                preview = fakeversion.compileContent(data=variables)
            except Exception as e:
                preview = "Failed to render preview. Error: %s" % str(e)
            resp = HttpResponse(preview)
            resp.__setitem__('X-XSS-Protection', '0')
            return resp

        else:
            if (
                thisformset.is_valid() and
                layerform.is_valid() and
                versionform.is_valid()
            ):
                request.session['newlysaved'] = True
                if layer_id is None:
                    newlayer = layerform.save(commit=False)
                    if newlayer.id is None:
                        newlayer.owner = request.user
                    newlayer.save()
                    layerform.save_m2m()
                    newversion = versionform.save(commit=False)
                    newversion.changes = "First Version."
                    newversion.last_edit = request.user
                    newversion.layer = newlayer
                    version = newversion
                    version.save()
                    instances = thisformset.save(commit=False)
                    for obj in thisformset.deleted_objects:
                        obj.delete()
                    for instance in instances:
                        instance.version = newversion
                        instance.save()
                    if request.POST.get('publish', False):
                        version.publish = True
                        version.save(publish=True)
                    return HttpResponseRedirect(
                        '/widgets/edit/%s/' % newlayer.id
                    )
                else:
                    layerform.save()
                    version = versionform.save(commit=False)
                    version.save()
                    instances = thisformset.save()
                    if request.POST.get('publish', False):
                        if Version.objects.filter(
                            layer=layer,
                            layer__shared=True,
                            publish=True
                        ).count() == 0 and Version.objects.filter(
                            layer=layer
                        ).count() == 1:
                            version.changes = "First Version."
                        else:
                            version.changes = request.POST.get('changes', '')
                        version.last_edit = request.user
                        version.publish = True
                        version.save(publish=True)
                    return HttpResponseRedirect('/widgets/edit/%s/' % layer.id)
            else:
                logger.error(
                    'Invalid form(s). %s -- %s -- %s' %
                    (versionform.errors, thisformset.errors, layerform.errors)
                    )
    context = {
        'layerform': layerform,
        'versionform': versionform,
        'controlsform': thisformset,
        'helper': ControlsInlineHelper(),
        'trhelper': ControlsRowHelper(),
        'layer': layer,
        'version': version,
        # 'layercodeform': layercodeform,
        'feedback': feedback,
        'feedback_count': feedback_count,
        # 'feedback_versions': sorted(feedback.keys(), reverse=True) if feedback != {} else None
    }
    return render(request, 'layers/editlayernew.html', context)


@login_required
@vary_on_cookie
def loadInput(request, id, force_input_id="", force_input_name="", control_id=None):
    thisinput = get_object_or_404(InputType, pk=id)
    if control_id is None:
        return HttpResponse(Template(thisinput.html).render(input_id=force_input_id, input_name=force_input_name))


@csrf_exempt
def layerSettings(request, version_id):
    if request.method != "POST":
        raise Http404
    version = get_object_or_404(Version, pk=version_id)
    controls = []
    for control in version.control_set.all():
        template = Template(control.input_obj.html)
        controls.append(
            template.render(
                control_title=control.title,
                control_verbose=control.input_obj.process_verbose(control.verbose),
                control_variable=control.variable_name,
                control_value=request.POST.get(control.variable_name, control.default)
            )
        )

    return render(request, 'slides/layersettings.html', {'controls': controls})


@login_required
def cloneSlide(request, slide_id):
    slide = get_object_or_404(Slide, pk=slide_id)
    # NEED TO VERIFY USER HAS VIEW PERMISSION!
    newslide = slide.clone(request.user)
    return HttpResponseRedirect('/slides/edit/%s/' % newslide.id)


@login_required
@vary_on_cookie
def manageSlides(request):
    # for now get all official layers and all user layers
    return render(request, 'slides/slides.html')


@login_required
@vary_on_cookie
def listSlides(request, mode):

    slides = Slide.objects.filter(owner=request.user).order_by('-edit_date')
    slideshow_ids = []

    # get list of corresponding slideshows per slide
    for slide in slides:
        slideshows = SlideshowSlide.objects.filter(slide_id=slide.id)
        slideshow_list = list()
        for slideshow in slideshows:
            title = Slideshow.objects.filter(id=slideshow.slideshow_id)
            if len(title) > 0 and slideshow.slideshow_id not in slideshow_ids:
                slideshow_list.append({"title": title[0].title, "id": slideshow.slideshow_id})
                slideshow_ids.append(slideshow.slideshow_id)

            # split slide list into 2 parts for display
            slide.slideshow_part1 = slideshow_list[:min(3, len(slideshow_list))]
            if len(slideshow_list) >= 3: slide.slideshow_part2 = slideshow_list[3:]

    groups = request.user.contentgroup_set.all() | request.user.owned_groups.all()
    groups = groups.distinct().annotate(c=Count('slides')).filter(c__gt=0)

    return render(request, 'slides/slide_list.html', {'slides': slides, 'groups': groups, 'mode': mode})


# Create your views here.
@login_required
@vary_on_cookie
def editSlide(request, slide_id=None):
    prev = request.GET.get('preview', False)
    # Keep track of if we saved or not..
    savestatus = None

    slide = None if slide_id is None else get_object_or_404(Slide, pk=slide_id)

    # Verify the user can edit this
    if slide is not None:
        if not get_item_permissions(request.user, slide).get('edit', False):
            raise Http404

    form = SlideForm() if slide is None else SlideForm(instance=slide)
    slidelayer_factory = inlineformset_factory(Slide, SlideLayer, form=SlideLayerForm, can_delete=True, extra=0)
    slidelayerset = slidelayer_factory(prefix="layer") if slide is None else slidelayer_factory(prefix="layer", instance=slide)

    if request.method == "POST":
        # Assume save failed unless proven otherwise.
        savestatus = False

        # Load data into forms...
        slidelayerset = slidelayer_factory(data=request.POST, prefix="layer") if slide is None else slidelayer_factory(data=request.POST, prefix="layer", instance=slide)
        form = SlideForm(data=request.POST) if slide is None else SlideForm(data=request.POST, instance=slide)

        # Generate Preview
        if prev is not False:
            slidelayerset.is_valid()
            form.is_valid()

            slidelayers = []
            for instance in slidelayerset:
                if instance.cleaned_data.get('DELETE') is False:
                    slidelayer = instance.save(commit=False)
                    slidelayers.append((slidelayer, renderSlideLayer(slidelayer)))

            return render(request, 'slides/sliderender.html', {'slidelayers': slidelayers})

        # Check if everything is OK
        if slidelayerset.is_valid() and form.is_valid():
            savestatus = True
            # Handle the slide
            newslide = form.save(commit=False)
            if slide is None:
                newslide.owner = request.user
            newslide.last_edit = request.user
            newslide.save()

            # Handle the slidelayers
            instances = slidelayerset.save(commit=False)
            for obj in slidelayerset.deleted_objects:
                obj.delete()
            for instance in instances:
                instance.slide = newslide
                instance.save()
            request.session['newlysaved'] = True
            if 'slide_cache_{}'.format(newslide.id) in cache:
                cache.delete('slide_cache_{}'.format(newslide.id))
            # print('gonna test expire')
            # expire_view_cache(request, '/slides/view/%s/' % newslide.id)
            # expire_view_cache(request, '/slides/view/%s/?preview=true' % newslide.id)
            return HttpResponseRedirect('/slides/edit/%s/' % newslide.id)

    context = {'layers': Layer.objects.all(), 'form': form, 'layerset': slidelayerset, 'slide': slide, 'savestatus': savestatus}
    return render(request, 'slides/canvas.html', context)


@login_required
@vary_on_cookie
def deleteSlideshow(request, slideshow_id):
    slideshow = get_object_or_404(Slideshow, pk=slideshow_id)
    if request.method != 'POST' or slideshow.owner != request.user:
            raise Http404
    else:
        slideshow.delete()
        return HttpResponse('SUCCESS')

@login_required
@vary_on_cookie
def deleteSlide(request, slide_id):
    slide = get_object_or_404(Slide, pk=slide_id)
    if request.method != 'POST' or slide.owner != request.user:
            raise Http404
    else:
        slide.delete()
        return HttpResponse('SUCCESS')

@login_required
@vary_on_cookie
def deleteLayer(request, layer_id):
    layer = get_object_or_404(Layer, pk=layer_id)
    if request.method != 'POST' or layer.owner != request.user:
            raise Http404
    else:
        layer.delete()
        return HttpResponse('SUCCESS')

def newLayerJson(request, version_id):
    version = get_object_or_404(Version, pk=version_id)
    data = {"versionid": version.id, "title": version.layer.title, 'short': version.layer.short, "data": {}}
    for control in version.control_set.all():
        data['data'][control.variable_name] = control.default
    return JsonResponse(data, safe=True)

@csrf_exempt
def fieldValidation(request):
    pass

# def expire_view_cache(request, path, key_prefix=None):
#     print('expire called', request, path)
#     """
#     This function allows you to invalidate any item from the per-view cache.
#     It probably won't work with things cached using the per-site cache
#     middleware (because that takes account of the Vary: Cookie header).
#     This assumes you're using the Sites framework.
#     Arguments:
#         * path: The URL of the view to invalidate, like `/blog/posts/1234/`.
#         * key prefix: The same as that used for the cache_page()
#           function/decorator (if any).
#     """

#     # Prepare metadata for our fake request.
#     # I'm not sure how 'real' this data needs to be, but still:

#     domain_parts = request.META['HTTP_HOST'].split(':')
#     request_meta = {'SERVER_NAME': domain_parts[0],}
#     if len(domain_parts) > 1:
#         request_meta['SERVER_PORT'] = domain_parts[1]
#     else:
#         request_meta['SERVER_PORT'] = '80'

#     # Create a fake request object

#     request = HttpRequest()
#     request.method = 'GET'
#     request.META = request_meta
#     request.path = path
#     print('request.path', request.path, request)

#     if settings.USE_I18N:
#         request.LANGUAGE_CODE = settings.LANGUAGE_CODE

#     # If this key is in the cache, delete it:
#     print('try expire')
#     try:
#         cache_key = get_cache_key(request, key_prefix=key_prefix)
#         print('cache key?', request, cache_key)
#         if cache_key:
#             print('has cachekey')
#             if cache.has_key(cache_key):
#                 cache.delete(cache_key)
#                 print('cache deleted')
#                 return (True, 'Successfully invalidated')
#             else:
#                 return (False, 'Cache_key does not exist in cache')
#         else:
#             print('no cachekey')
#             raise ValueError('Failed to create cache_key')
#     except (ValueError, Exception) as e:
#         return (False, e)


@cache_page(60*15)
def viewSlide(request, slide_id):
    preview = request.GET.get('preview', False)
    preview = request.GET.get('overlay', False)
    cache_key = 'slide_cache_{}'.format(slide_id)
    if not preview:
        content = cache.get(cache_key)
        if content:
            return HttpResponse(content)
    slide = get_object_or_404(Slide.everything, pk=slide_id)
    slidelayers = []
    aspect = [int(x) for x in slide.aspect_ratio.split('x')]
    if aspect[0] > aspect[1]:
        dimensions = [100, 100.0 * aspect[1] / aspect[0]]
    else:
        dimensions = [100.0 * aspect[0] / aspect[1], 100.0 * aspect[1] / aspect[0]]
    for instance in slide.slidelayer_set.all():
        if instance.version is None:
            slidelayers.append((instance, renderSlideLayer(None, instance.data)))
        else:
            slidelayers.append((instance, renderSlideLayer(instance.version.id, instance.data)))
    content = loader.render_to_string(
        'slides/sliderender.html',
        {'slide': slide, 'slidelayers': slidelayers, 'dimensions': dimensions},
        request=request
    )
    if not preview:
        cache.set(cache_key, content, 60*15)

    return HttpResponse(content)


    # slide = get_object_or_404(Slide.everything, pk=slide_id)
    # slidelayers = []
    # aspect = [int(x) for x in slide.aspect_ratio.split('x')]
    # if aspect[0] > aspect[1]:
    #     dimensions = [100, 100.0 * aspect[1] / aspect[0]]
    # else:
    #     dimensions = [100.0 * aspect[0] / aspect[1], 100.0 * aspect[1] / aspect[0]]
    # for instance in slide.slidelayer_set.all():
    #     slidelayers.append((instance, renderSlideLayer(instance.version.id, instance.data)))

    # # print('cached?', cache.get('/slides/view/19/'), get_cache_key(request), expire_view_cache(request, '/slides/view/19/'))
    # return render(request, 'slides/sliderender.html', {'slide': slide, 'slidelayers': slidelayers, 'dimensions': dimensions})


@csrf_exempt
@vary_on_cookie
def previewLayer(request):
    if request.method == "POST":
        return HttpResponse(renderSlideLayer(request.POST.get('version'), request.POST.get('data', None)))


def renderSlideLayer(versionid, data):
    if versionid == None:
        return data
    version = Version.objects.get(pk=versionid)
    try:
        data = json.loads(data)
    except:
        pass
    if data == {} or data == 'null' or data == '"{}"':
        data = None
    return version.compileContent(data)


@login_required
def cloneSlideshow(request, slideshow_id):
    slideshow = get_object_or_404(Slideshow, pk=slideshow_id)
    # NEED TO VERIFY USER HAS VIEW PERMISSION!
    newslideshow = slideshow.clone(request.user)
    return HttpResponseRedirect('/slideshows/edit/%s/' % newslideshow.id)


@login_required
@vary_on_cookie
def manageSlideshows(request):
    # for now get all official layers and all user layers
    return render(request, 'slideshows/slideshows.html')


@login_required
@vary_on_cookie
def listSlideshows(request, mode):
    slideshows = Slideshow.objects.filter(owner=request.user).order_by('-edit_date')

    groups = request.user.contentgroup_set.all() | request.user.owned_groups.all()
    groups = groups.distinct().annotate(c=Count('slideshows')).filter(c__gt=0)
    return render(request, 'slideshows/slideshow_list.html', {'slideshows': slideshows, 'mode': mode, 'groups': groups})

def convert_pdf_to_slideshow(request, f):

    base_image_name, extension = os.path.splitext(f.name)

    # Check the extension to see if the file is a PDF
    # if it's not reject and return False and the error message
    if extension != ".pdf":
        return False, -1, "This file is not a PDF. Only PDFs are accepted."

    # Retrieving slide titles and slide durations from the form
    slideshow_title = base_image_name if request.POST["slideshow_title"] == "" else request.POST["slideshow_title"]
    slide_duration = request.POST['slide_duration']
    slideshow_description = f"Slideshow created from {base_image_name}" if request.POST["slideshow_description"] == "" else request.POST["slideshow_description"]

    # Convert the pdf to images
    images = convert_from_bytes(f.read(), fmt="png")

    # Create Slide Show Object
    slideshow = Slideshow.objects.create(owner=request.user, title=slideshow_title, description=slideshow_description)
    slideshow.save()
    slideshow_id = slideshow.id

    # Upload each image
    # Create a slide for each image
    # Create a slide layer for the slide containing the uploaded image
    # Add the slide to the slideshow
    for count, image in enumerate(images):

        # Create image name from the name and count
        image_name =  f"uploaded_images/{base_image_name}_{count}.png" # Create image name using count + file name

        # Things break if the media url has a slash at the beginning (ie. /media/)
        # Handling this case
        media_url = settings.MEDIA_URL if settings.MEDIA_URL[0] != "/" else settings.MEDIA_URL[1:]

        full_image_path = f"{media_url}{image_name}"
        image.save(full_image_path, 'PNG')

        # Save the image in the database
        uim = UploadedImage.objects.create(image=image_name, owner=request.user)
        uim.save()

        # Create a Slide
        slide = Slide.objects.create(owner=request.user, title=f"{base_image_name}_{count}", description=f"{uim.image.url}", data="")
        slide.save()

        slideLayer = SlideLayer.objects.create(slide=slide, title="Image", data=f"<img style=\"max-width: 80%; max-height: 80%; object-fit: contain;\" src=\"../../../{full_image_path}\">", index=1, x=10, y=0, width=100, height=100, version_id=None)
        slideLayer.save()

        # Adding Slide to Slideshow
        slideshowSlide = SlideshowSlide.objects.create(slideshow=slideshow, slide=slide, order=count, length=slide_duration)
        slideshowSlide.save()
    # If no errors occurred, return True to indicate
    # a successful run
    return True, slideshow_id, "",


def uploadPDF(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if request.FILES['document']:
            success, slideshow_id, error_msg = convert_pdf_to_slideshow(request, request.FILES['document'])

            # Redirect to the completed slideshow if successful
            if success:
                return HttpResponseRedirect(f'/slideshows/edit/{slideshow_id}/')
            else:
                # Rerender the page but with the error message
                return render(request, 'slideshows/create_slideshow_from_pdf.html', {'error': True, 'error_msg': error_msg})
    else:
        form = UploadFileForm()
    return HttpResponseRedirect('/slideshows/pdf/')

@login_required
def createSlideshowPDF(request):
    return render(request, 'slideshows/create_slideshow_from_pdf.html')

@login_required
@vary_on_cookie
def editSlideshow(request, slideshow_id=None):
    savestatus = None

    slideshow = None if slideshow_id is None else get_object_or_404(Slideshow, pk=slideshow_id)

    # Verify the user can edit this
    if slideshow is not None:
        if not get_item_permissions(request.user, slideshow).get('edit', False):
            raise Http404

    form = SlideshowForm() if slideshow is None else SlideshowForm(instance=slideshow)
    # Clean up old slides...
    if slideshow:
        deleted = SlideshowSlide.objects.filter(slideshow=slideshow, time_end__lt=timezone.now(), end_removal=True)
        d_count = deleted.count()
        deleted.delete()
        if d_count > 0:
            for i, ss in enumerate(SlideshowSlide.objects.filter(slideshow=slideshow)):
                ss.order = i
                ss.save()
    slideset_factory = inlineformset_factory(Slideshow, SlideshowSlide, form=SlideshowSlideForm, can_delete=True, extra=0)
    slideset = slideset_factory(prefix="slide") if slideshow is None else slideset_factory(prefix="slide", instance=slideshow)

    if request.method == "POST":
        form = SlideshowForm(data=request.POST) if slideshow is None else SlideshowForm(data=request.POST, instance=slideshow)
        slideset = slideset_factory(data=request.POST, prefix="slide") if slideshow is None else slideset_factory(data=request.POST, prefix="slide", instance=slideshow)
        if slideset.is_valid() and form.is_valid():
            newslideshow = form.save(commit=False)
            if newslideshow.id is None:
                newslideshow.owner = request.user
            newslideshow.last_edit = request.user
            newslideshow.save()

            instances = slideset.save(commit=False)
            for obj in slideset.deleted_objects:
                obj.delete()
            for instance in instances:
                instance.slideshow = newslideshow
                instance.save()
            request.session['newlysaved'] = True
            return HttpResponseRedirect('/slideshows/edit/%s/' % newslideshow.id)
        else:
            logger.error('Invalid form and/or slideset.')

    context = {'slides': Slide.objects.all(), 'form': form, 'slideset': slideset, 'slideshow': slideshow, 'savestatus': savestatus}
    return render(request, 'slideshows/editslideshow.html', context)


def viewSlideshow(request, slideshow_id):
    slideshow = get_object_or_404(Slideshow, pk=slideshow_id)

    # for slide in slideshow.slideshowslide_set.filter(slide__trashed_at__isnull=True):
    #   print('trashed boi', slide.title)

    first_slide = slideshow.slideshowslide_set.filter(
        (Q(time_end__isnull=True)|Q(time_end__gt=timezone.now()))&((Q(time_start__isnull=True)|Q(time_start__lt=timezone.now())))
    ).order_by('order').first()
    if first_slide:
        aspect = [int(x) for x in first_slide.slide.aspect_ratio.split('x')]
        if aspect[0] > aspect[1]:
            dimensions = [100, 100.0 * aspect[1] / aspect[0]]
        else:
            dimensions = [100.0 * aspect[0] / aspect[1], 100.0 * aspect[1] / aspect[0]]
    else:
        dimensions = [100, 56.25]
        return render(request, 'slideshows/slideshow_no_slides_found.html')
    context = {
        'dimensions': dimensions,
        'slideshow': slideshow,
        'fs_src': '/slides/view/%s/' % first_slide.slide.id,
        'fs_time': first_slide.length * 1000,
    }
    if slideshow.overlay:
        context['overlay_src'] = '/slides/view/%s/?overlay=true' % slideshow.overlay.id
    return render(
        request,
        'slideshows/slideshowrender.html',
        context
    )


def nextSlide(request, slideshow_id, current_index):
    current_index = int(current_index)
    slideshow = get_object_or_404(Slideshow, pk=slideshow_id)
    slideshowslides = list(slideshow.slideshowslide_set.filter(
        (Q(time_end__isnull=True)|Q(time_end__gt=timezone.now()))&((Q(time_start__isnull=True)|Q(time_start__lt=timezone.now())))
    ).order_by('order'))
    old_slide = slideshowslides[current_index]
    next_slide = None
    index = current_index + 1
    for slide in slideshowslides[current_index + 1:]:
        if slide.time_start is None or slide.time_start < timezone.now():
            if slide.time_end is None or slide.time_end > timezone.now():
                next_slide = slide
                break
        index += 1

    if next_slide is None:
        index = 0
        for slide in slideshowslides[:current_index]:
            if slide.time_start is None or slide.time_start < timezone.now():
                if slide.time_end is None or slide.time_end > timezone.now():
                    next_slide = slide
                    break
            index += 1

    if next_slide is None:
        next_slide = old_slide
        index = current_index


    # try:
    #   # loop through slides until we get one we can use...
    #   for i in slideshowslide_set.all().order_by('order')[index+1:]:

    #     next_slide = slideshow.slideshowslide_set.get(order=int(current_index) + 1)
    #     index = int(current_index) + 1
    # except Exception, e:
    #     print str(e)
    #     next_slide = slideshow.slideshowslide_set.get(order=0)
    #     index = 0
    return JsonResponse({'slide': '/slides/view/%s/' % next_slide.slide.id, 'time': next_slide.length * 1000, 'index': index})



###
# Group Management
###
@login_required
@vary_on_cookie
def manageGroups(request):
    return render(request, 'groups/groups.html')


@login_required
@vary_on_cookie
def listGroups(request, mode):
    groups = ContentGroup.objects.filter(owner=request.user).order_by('title')
    other_groups = ContentGroup.objects.filter(members=request.user)
    return render(request, 'groups/group_list.html', {'groups': groups, 'other_groups': other_groups, 'mode': mode})


@login_required
@vary_on_cookie
def editGroup(request, group_id=None):
    group = None if group_id is None else get_object_or_404(ContentGroup, pk=group_id)
    owner = False
    group_owner = False
    if group:
        if not (group.owner == request.user or \
            GroupMembership.objects.filter(contentgroup=group, user=request.user).first().can_add_users or \
            GroupMembership.objects.filter(contentgroup=group, user=request.user).first().can_remove_users):
            raise Http404
        owner = group.owner == request.user
        group_owner = group.owner
    perms = group_perms(request.user, group_id)
    form = GroupForm() if group is None else GroupForm(instance=group)
    memberset_factory = inlineformset_factory(ContentGroup, GroupMembership, form=GroupMembershipForm, can_delete=True, extra=0)
    memberset = memberset_factory(prefix="member") if group is None else memberset_factory(prefix="member", instance=group)

    if request.method == "POST":
        # Alter the post data: Convert all user names to ID numbers and create any users necessary
        form = GroupForm(request.POST) if group is None else GroupForm(request.POST, instance=group)
        memberset = memberset_factory(data=request.POST, prefix="member") if group is None else memberset_factory(data=request.POST, prefix="member", instance=group)
        if form.is_valid() and memberset.is_valid():
            newgroup = form.save(commit=False)
            if newgroup.id is None:
                newgroup.owner = request.user
            if newgroup.owner == request.user or request.user.is_superuser:
                newgroup.save()

            memberset.save(commit=False)
            if perms and perms['can_remove_users'] or newgroup.owner == request.user:
                for obj in memberset.deleted_objects:
                    obj.delete()
            for membership in memberset:
                # Prevents recreating groupmemberships and creating empty ones, respectively
                if not membership in memberset.deleted_forms and membership.cleaned_data:
                    username = membership.cleaned_data['username']
                    if GroupMembership.objects.filter(contentgroup=group, user__username=username).count() == 0 and perms and perms['can_add_users'] \
                    or GroupMembership.objects.filter(contentgroup=group, user__username=username).count() == 0 and newgroup.owner == request.user:
                        instance = membership.save(commit=False)
                        instance.contentgroup = newgroup
                        instance.user = User.objects.get_or_create(username=username[:8] if len(username) > 8 else username)[0]
                        if not newgroup.owner == request.user:
                            if group and not perms['can_edit_permissions']:
                                instance.can_add_users = False
                                instance.can_remove_users = False
                                instance.can_use_items = False
                                instance.can_add_items = False
                                instance.can_remove_items = False
                                instance.can_edit_items = False
                                instance.can_edit_permissions = False
                        instance.save()
                    if GroupMembership.objects.filter(contentgroup=group, user__username=username).count() == 1 and perms and perms['can_edit_permissions'] \
                    or GroupMembership.objects.filter(contentgroup=group, user__username=username).count() == 1 and newgroup.owner == request.user:
                        gm = GroupMembership.objects.get(contentgroup=group, user__username=username)
                        gm = membership.save()
            request.session['newlysaved'] = True
            return HttpResponseRedirect('/groups/edit/%s/' % newgroup.id)

    for item in memberset:
        if item.instance and item.initial:
            item.initial['username'] = item.instance.user.username

    context = {
        'form': form,
        'group': group,
        'helper': ControlsInlineHelper(),
        'trhelper': ControlsRowHelper(),
        'membersform': memberset,
        'owner': owner,
        'group_owner': group_owner,
        'perms': perms
    }
    return render(request, 'groups/editgroup.html', context)

@login_required
@vary_on_cookie
def viewGroup(request, group_id):
    group = get_object_or_404(ContentGroup, pk=group_id)
    # Check if this person can see this group probably
    if GroupMembership.objects.filter(contentgroup=group, user=request.user) or group.owner == request.user:
        return render(request, 'groups/view.html', {'group': group, 'mode': 'manage'})
    else:
        raise Http404


@login_required
@vary_on_cookie
def transferOwnership(request, item, pk):
    # Get the item and identify if the user is the owner
    itemtype = item
    if item == 'slide':
        item = get_object_or_404(Slide, pk=pk)
    elif item == 'layer':
        item = get_object_or_404(Layer, pk=pk)
    elif item == 'slideshow':
        item = get_object_or_404(Slideshow, pk=pk)
    elif item == 'screen':
        item = get_object_or_404(Screen, pk=pk)
    elif item == 'group':
        item = get_object_or_404(ContentGroup, pk=pk)
    else:
        raise Http404

    if request.method == 'POST':
        # Check that we have a valid recipient:
        recipient = request.POST.get('recipient', None)
        if recipient is not None:
            recipient = User.objects.filter(username=recipient.lower()).first()
        if recipient is not None and recipient != request.user:
            # If this is a group we need to do some fancy finagling...
            if itemtype == 'group':
                # If the recipient is already in the group, remove their current ownership...
                membership = item.groupmembership_set.filter(user=recipient).first()
                if membership is not None:
                    membership.delete()
                # Make the current owner a new fully-permissive membership in the group
                GroupMembership.objects.create(
                    can_add_users=True,
                    can_remove_users=True,
                    can_use_items=True,
                    can_add_items=True,
                    can_remove_items=True,
                    can_edit_items=True,
                    contentgroup=item,
                    user=request.user
                )
                if request.POST.get('transfer_all_items', '') == 'true':
                    # Transfer everything
                    # item.layers.filter(owner=request.user).update(owner=recipient)
                    item.screens.filter(owner=request.user).update(owner=recipient)
                    item.slides.filter(owner=request.user).update(owner=recipient)
                    item.slideshows.filter(owner=request.user).update(owner=recipient)
            item.owner = recipient
            item.save()

            return HttpResponse('SUCCESS')
        return HttpResponse('FAILED')

    return render(request, 'groups/transfer.html', {'item': item, 'itemtype': itemtype})


@login_required
@vary_on_cookie
def shareSettings(request, item, pk):
    # Get the item and identify if the user is the owner
    itemtype = item
    if item == 'slide':
        item = get_object_or_404(Slide, pk=pk)
    elif item == 'layer':
        item = get_object_or_404(Layer, pk=pk)
    elif item == 'slideshow':
        item = get_object_or_404(Slideshow, pk=pk)
    elif item == 'screen':
        item = get_object_or_404(Screen, pk=pk)
    else:
        raise Http404

    if request.method == 'POST':
        # First verify that this user can even do this..
        if request.user == item.owner:  # OR WHATEVER OTHER RULES APPLY FOR SHARING THIS SHIT
            # Get all the items in the list and compare them with the groups the item is already in
            oldgroups = set([x.id for x in item.contentgroup_set.all()])
            newgroups = set([int(x) for x in request.POST.getlist('data[]')])
            adding = list(newgroups - oldgroups)
            removing = list(oldgroups - newgroups)
            for groupid in adding:
                # NEED TO VERIFY THAT THE USER CAN DO THESE THINGS PLS
                item.contentgroup_set.add(ContentGroup.objects.get(pk=groupid))
            for groupid in removing:
                item.contentgroup_set.remove(ContentGroup.objects.get(pk=groupid))
            return HttpResponse('SUCCESS')
        raise Http404
    else:
        # Get everything this item is currently shared with, and determine if the user can delete it....
        groups = [{'title': x.title, 'id': x.id, 'candelete': True if item.owner == request.user else False} for x in item.contentgroup_set.all()]
        context = {'item': item, 'itemtype': itemtype, 'groups': json.dumps(groups)}

        return render(request, 'groups/share_modal.html', context)


@login_required
@vary_on_cookie
def manageScreens(request):
    # for now get all official layers and all user layers
    return render(request, 'screens/screens.html')


@login_required
@vary_on_cookie
def listScreens(request):
    screens = Screen.objects.filter(owner=request.user).order_by('-last_connect')
    groups = request.user.contentgroup_set.all() | request.user.owned_groups.all()
    groups = groups.distinct().annotate(c=Count('screens')).filter(c__gt=0)
    return render(request, 'screens/screen_list.html', {'screens': screens, 'groups': groups})


@login_required
@vary_on_cookie
def editScreen(request, screen_id=None):
    savestatus = None

    screen = None if screen_id is None else get_object_or_404(Screen, pk=screen_id)
    item = None
    itemtype = None

    if screen is not None:
        linksplit = screen.link.split('/')
        itemtype = linksplit[1]
        if itemtype == 'slides':
            try:
                item = Slide.objects.get(pk=int(linksplit[3]))
            except:
                item = None
        elif itemtype == 'slideshows':
            try:
                item = Slideshow.objects.get(pk=int(linksplit[3]))
            except:
                item = None

    # Verify the user can edit this
    if screen is not None:
        if not get_item_permissions(request.user, screen).get('edit', False):
            raise Http404

    form = ScreenForm() if screen is None else ScreenForm(instance=screen)

    if request.method == 'POST':
        form = ScreenForm(request.POST) if screen is None else ScreenForm(request.POST, instance=screen)
        if form.is_valid():
            s = form.save(commit=False)
            if s.id is None:
                s.owner = request.user
            s.save()
            request.session['newlysaved'] = True
            return HttpResponseRedirect('/screens/edit/%s/' % s.id)

    return render(request, 'screens/editscreen.html', {'screen': screen, 'form': form, 'itemtype': itemtype, 'item': item, 'mode': 'manage'})


def viewScreen(request, slug):
    print(('request', request.GET))
    if slug.isdigit():
        screen = get_object_or_404(Screen, id=slug)
    else:
        screen = get_object_or_404(Screen, slug=slug)
    now = datetime.now()
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip_address = x_forwarded_for.split(',')[0]
    else:
        ip_address = request.META.get('REMOTE_ADDR')
    screen.last_connect = now
    screen.ip = ip_address
    screen.save()
    dimensions = [100, 56.25]
    return render(request, 'screens/viewscreen.html', {'screen': screen, 'dimensions': dimensions})


def pingScreen(request, slug):
    screen = get_object_or_404(Screen, slug=slug)
    now = timezone.now()
    linksplit = screen.link.split('/')
    itemtype = linksplit[1]
    slide_has_updated = False
    if itemtype == 'slides':
        slide = Slide.objects.get(pk=int(linksplit[3]))
        slide_has_updated = screen.last_connect < slide.edit_date < now
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip_address = x_forwarded_for.split(',')[0]
    else:
        ip_address = request.META.get('REMOTE_ADDR')
    screen.last_connect = now
    screen.ip = ip_address
    screen.save()
    if settings.EMERGENCY_ALERTS_ENABLED:
        emergency = queryEmergency(request)
        if emergency:
            return emergency
    return JsonResponse({'target': screen.link})

#trashbin
def trashbin(request):
    trash_slide_list = Slide.trash.filter(owner=request.user).order_by('-trashed_at')
    trash_layer_list = Layer.trash.filter(owner=request.user).order_by('-trashed_at')
    trash_SlideLayer_list = SlideLayer.objects.all()
    trash_slideshow_list = Slideshow.trash.filter(owner=request.user).order_by('-trashed_at')
    context = {'trash_slide_list': trash_slide_list, 'trash_layer_list': trash_layer_list, 'trash_SlideLayer_list': trash_SlideLayer_list, 'trash_slideshow_list': trash_slideshow_list}
    return render(request, 'trashbin/trashbin.html', context)

def delete(request, model, id):
    if model == "slide":
        thing = Slide.trash.get(pk=id)
    elif model == 'layer':
        thing = Layer.trash.get(pk=id)
    elif model == 'slideshow':
        thing = Slideshow.trash.get(pk=id)
    elif model == 'uploadedImage':
        thing = UploadedImage.get(pk=id)
    else:
        logger.info('No model matching query found.')
    if thing.owner == request.user:
        thing.delete()
    else:
        raise Http404

    return HttpResponseRedirect('/trashbin/')

def restore(request, model, id):
    if model == "slide":
        thing = Slide.trash.get(pk=id)
    elif model == 'layer':
        thing = Layer.trash.get(pk=id)
    elif model == 'slideshow':
        thing = Slideshow.trash.get(pk=id)
    else:
        logger.info('No model matching query found.')
    if thing.owner == request.user:
        thing.restore()
    else:
        raise Http404

    return HttpResponseRedirect('/trashbin/')

# Images stuff
def images(request):
    context = {'images': request.user.uploadedimage_set.order_by('-creation_date')}
    return render(request, 'images/images.html', context)

def imageUpload(request):
    return render(request, 'images/image_upload_only.html')

def deleteImage(request, id):
    thing = request.user.uploadedimage_set.get(pk=id)
    if thing.owner == request.user:
        thing.delete()
    else:
        logger.warning('Cannot delete.')
    return HttpResponseRedirect('/images/')


# Video stuff
def videos(request):
    context = {'videos': request.user.uploadedvideo_set.order_by('-creation_date')}
    return render(request, 'videos/videos.html', context)


def videoUpload(request):
    return render(request, 'videos/video_upload_only.html')


def deleteVideo(request, id):
    thing = request.user.uploadedvideo_set.get(pk=id)
    if thing.owner == request.user:
        thing.delete()
    else:
        logger.warning('Cannot delete.')
    return HttpResponseRedirect('/videos/')
