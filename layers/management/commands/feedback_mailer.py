from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.db.models import Q
from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from django.conf import settings
from layers.models import VersionFeedback
from django.utils import timezone
from datetime import timedelta
from collections import defaultdict
from scinage.settings_local import SITE_URL


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        self.stdout.write('Beginning process of writing emails...')

        feedback = VersionFeedback.objects.filter(creation_date__gte=timezone.now()-timedelta(days=7)).order_by('version__layer', 'feedback_type', '-creation_date').prefetch_related('version__layer__owner')
        d = defaultdict(list)
        for f in feedback:
            d[f.version.layer.owner].append(f)

        for user, feedbacks in d.items():

            context = {
                'feedback': feedbacks,
                'url': SITE_URL,
            }

            html = render_to_string(
                'feedback_email_template.html',
                context
            )
            if user.username:
                msg = EmailMessage(
                    'Scinage Widget Feedback',
                    html,
                    'scialert@uwaterloo.ca',
                    ['%s@uwaterloo.ca' % user.username],
                )
                msg.content_subtype = "html"
                msg.send(fail_silently=True)
