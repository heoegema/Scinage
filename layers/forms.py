from django import forms
from .models import Control, Layer, Version, Slide, SlideLayer, Slideshow, \
SlideshowSlide, ContentGroup, GroupMembership, Screen, VersionFeedback
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Div, Layout, Fieldset, HTML, Field
from codemirror import CodeMirrorTextarea
from django.utils import formats
from django.forms import BaseModelFormSet
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


form_show_labels = True

class UserCreateForm(UserCreationForm):
	email = forms.EmailField(required=True, label='Email', error_messages={'exists': 'Oops'})

	class Meta:
		model = User
		fields = ("username", "email", "password1", "password2")

	def save(self, commit=True):
		user = super(UserCreateForm, self).save(commit=False)
		user.email = self.cleaned_data["email"]
		if commit:
			user.save()
		return user

	def clean_email(self):
		if User.objects.filter(email=self.cleaned_data['email']).exists():
			raise ValidationError(self.fields['email'].error_messages['exists'])
		return self.cleaned_data['email']


class LocalDateTimeInput(forms.widgets.DateTimeInput):	
	usel10n=True


class ControlsInlineHelper(FormHelper):
	def __init__(self, *args, **kwargs):
		super(ControlsInlineHelper, self).__init__(*args, **kwargs)
		self.form_method = 'post'
		self.form_tag = False
		self.render_required_fields = True
		self.template = 'table_inline_template.html'


class ControlsRowHelper(FormHelper):
	def __init__(self, *args, **kwargs):
		super(ControlsRowHelper, self).__init__(*args, **kwargs)
		self.form_method = 'post'
		self.render_required_fields = True
		self.form_show_labels = False
		self.form_tag = False
		self.template = 'tr_template.html'


class ControlForm(forms.ModelForm):
	class Meta:
		model = Control
		fields = ["input_type", "title", "variable_name", "verbose", "default"]
		widgets = {'verbose': forms.TextInput()}


class LayerForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(LayerForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper(self)
		self.helper.form_tag = False
		self.helper.layout = Layout(
			Div(
				Div('title', css_class='col-lg-4 col-xs-4'),
				Div('short', css_class='col-lg-4 col-xs-4'),
				Div('description', css_class='col-lg-4 col-xs-4'),
				Div('icon', css_class='col-lg-4 col-xs-4'),
				Div('tags', css_class='col-lg-4 col-xs-4'),
				Div('shared', css_class='col-lg-2 col-xs-2'),
				# Div('publish', css_class='col-lg-2 col-xs-2'),
				css_class='row-fluid'),
			HTML("""<div id="changesmodal" style="z-index:30000;" class="modal fade"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button><h4 class="modal-title">Specify Changes</h4></div><div class="modal-body">"""),
			HTML("""<p>Specify your changes here. If you keep the 'publish' button checked and save this widget, the widget will be stored as the current 'stable' version of this widget. A new development widget will be created after that which can be worked on.</p>"""),
			Fieldset('', 'changes'),
			HTML("""</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Ok</button></div></div></div></div>"""),
			HTML("""<div id="summernote">"""),
			Fieldset('', 'documentation'),
			HTML("""</div>""")
		)
		self.fields['shared'].label = "Public"
		self.fields['documentation'].label = ''

	class Meta:
		model = Layer
		widgets = {
          'description': forms.Textarea(attrs={'rows':2}),
        }
		fields = ["title", "icon", "short", "description", "shared", "tags", "changes", "documentation"]


class VersionForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(VersionForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper(self)
		self.helper.include_media = False
		self.helper.form_tag = False
		content = forms.CharField(widget=CodeMirrorTextarea(
				mode='htmlmixed',
				dependencies=('javascript', 'xml', 'css'),
				config={
					'lineWrapping': True
				},
			)
		)
		content_js = forms.CharField(
			widget=CodeMirrorTextarea(mode='javascript', dependencies=('javascript', 'xml', 'css'),
				config={
					'lineWrapping': True
				},
			)
		)
		content_css = forms.CharField(
			widget=CodeMirrorTextarea(mode='css', dependencies=('javascript', 'xml', 'css'),
				config={
					'lineWrapping': True
				},
			)
		)

	class Meta:
		model = Version
		fields = ["content", "content_css", "content_js"]
		widgets = {
			"content": CodeMirrorTextarea(mode='htmlmixed', dependencies=('javascript', 'xml', 'css'),
				config={
					'lineWrapping': True
				}
			),
			"content_js": CodeMirrorTextarea(mode='javascript', dependencies=('javascript', 'xml', 'css'),
				config={
					'lineWrapping': True
				}
			),
			"content_css": CodeMirrorTextarea(mode='css', dependencies=('javascript', 'xml', 'css'),
				config={
					'lineWrapping': True
				}
			),
		}


class VersionFeedbackForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(VersionFeedbackForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper(self)
		self.helper.form_tag = False

	class Meta:
		model = VersionFeedback
		fields = ["feedback_type", "feedback"]


class SlideForm(forms.ModelForm):
	class Meta:
		model = Slide
		fields = ['title', 'description', 'aspect_ratio']
		widgets = {'description': forms.Textarea(attrs={'rows': 3})}

	def __init__(self, *args, **kwargs):
		super(SlideForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper(self)
		self.helper.form_tag = False


class SlideLayerForm(forms.ModelForm):
	class Meta:
		model = SlideLayer
		exclude = ['slide']
		widgets = {'version': forms.HiddenInput()}

	def __init__(self, *args, **kwargs):
		super(SlideLayerForm, self).__init__(*args, **kwargs)
		# Add a self-titled class to each item for easy reference in jquery
		# e.g field "index" will have class "index"
		for key, field in self.fields.items():
			field.widget.attrs['class'] = key


class SlideshowForm(forms.ModelForm):
	class Meta:
		model = Slideshow
		fields = ['title', 'description', 'overlay']

	def __init__(self, *args, **kwargs):
		super(SlideshowForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper(self)
		self.helper.form_tag = False


class SlideshowSlideForm(forms.ModelForm):
	class Meta:
		model = SlideshowSlide
		exclude = ['slideshow']
		widgets = {'slide': forms.HiddenInput()}

	def __init__(self, *args, **kwargs):
		super(SlideshowSlideForm, self).__init__(*args, **kwargs)
		self.fields["time_start"].widget = LocalDateTimeInput()
		self.fields["time_end"].widget = LocalDateTimeInput()

		# Add a self-titled class to each item for easy reference in jquery
		# e.g field "index" will have class "index"
		for key, field in self.fields.items():
			field.widget.attrs['class'] = key
		self.fields['length'].widget.attrs['class'] += ' form-control'
		self.fields['time_start'].widget.attrs['class'] += ' form-control timeselect'
		self.fields['time_end'].widget.attrs['class'] += ' form-control timeselect'


class GroupForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(GroupForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper(self)
		self.helper.form_tag = False

	class Meta:
		model = ContentGroup
		fields = ["title", "description"]


class GroupMembershipForm(forms.ModelForm):
	username = forms.CharField(required=True)
	class Meta:
		model = GroupMembership
		fields = [
			'username',
			'can_edit_permissions',
			'can_add_users',
			'can_remove_users',
			'can_use_items',
			'can_add_items',
			'can_edit_items',
			'can_remove_items',
		]
		exclude = ['contentgroup', 'user']
		widgets = {'user': forms.TextInput()}

	def __init__(self, *args, **kwargs):
		super(GroupMembershipForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper(self)
		self.helper.form_tag = False
		self.fields['can_edit_permissions'].widget.attrs['class'] = 'owner-only'


class ScreenForm(forms.ModelForm):
	def __init__(self, *args, **kwargs):
		super(ScreenForm, self).__init__(*args, **kwargs)
		self.helper = FormHelper(self)
		self.helper.form_tag = False

	class Meta:
		model = Screen
		exclude = ['ip', 'slug', 'owner', 'last_connect']
		widgets = {'link': forms.HiddenInput()}

class UploadFileForm(forms.Form):
	slideshow_title = forms.CharField(max_length=255)
	slide_duration = forms.IntegerField()
	file = forms.FileField()
	template_name = 'slideshows/create_slideshow_from_pdf.html'