from haystack import indexes
from layers.models import Layer


class LayerIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    official = indexes.BooleanField(model_attr='official')
    #owner = indexes.CharField(model_attr='user')
    tags = indexes.MultiValueField()

    def get_model(self):
        return Layer

    def prepare_tags(self, obj):
        return [tag.name for tag in obj.tags.all()]

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(shared=True)