# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('layers', '0008_layer_icon'),
    ]

    operations = [
        migrations.CreateModel(
            name='Station',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('edit_date', models.DateTimeField(auto_now=True, null=True)),
                ('last_edit', models.ForeignKey(related_name='station_last_edit', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
                ('ro_access', models.ManyToManyField(related_name='station_ro_users', to=settings.AUTH_USER_MODEL, blank=True)),
                ('rw_access', models.ManyToManyField(related_name='station_rw_users', to=settings.AUTH_USER_MODEL, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='StationItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('priority', models.IntegerField(default=1)),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('repeat', models.CharField(max_length=200)),
            ],
        ),
        migrations.AddField(
            model_name='layer',
            name='ro_access',
            field=models.ManyToManyField(related_name='layer_ro_users', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AddField(
            model_name='layer',
            name='rw_access',
            field=models.ManyToManyField(related_name='layer_rw_users', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AddField(
            model_name='slide',
            name='last_edit',
            field=models.ForeignKey(related_name='slide_edit_user', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='slide',
            name='ro_access',
            field=models.ManyToManyField(related_name='slide_ro_users', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AddField(
            model_name='slide',
            name='rw_access',
            field=models.ManyToManyField(related_name='slide_rw_users', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AddField(
            model_name='slideshow',
            name='last_edit',
            field=models.ForeignKey(related_name='slideshow_last_edit', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='slideshow',
            name='ro_access',
            field=models.ManyToManyField(related_name='slideshow_ro_users', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AddField(
            model_name='slideshow',
            name='rw_access',
            field=models.ManyToManyField(related_name='slideshow_rw_users', to=settings.AUTH_USER_MODEL, blank=True),
        ),
        migrations.AddField(
            model_name='version',
            name='edit_date',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AddField(
            model_name='version',
            name='last_edit',
            field=models.ForeignKey(related_name='version_last_edit', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE),
        ),
        migrations.AlterField(
            model_name='slide',
            name='edit_date',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AlterField(
            model_name='slideshow',
            name='edit_date',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AddField(
            model_name='stationitem',
            name='slide',
            field=models.ForeignKey(blank=True, to='layers.Slide', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='stationitem',
            name='slideshow',
            field=models.ForeignKey(blank=True, to='layers.Slideshow', null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='stationitem',
            name='station',
            field=models.ForeignKey(to='layers.Station', on_delete=models.CASCADE),
        ),
    ]
