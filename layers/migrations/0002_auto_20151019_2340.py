# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='inputtype',
            name='validator',
            field=models.CharField(default=0, max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='inputtype',
            name='validator_error',
            field=models.CharField(default=0, max_length=255),
            preserve_default=False,
        ),
    ]
