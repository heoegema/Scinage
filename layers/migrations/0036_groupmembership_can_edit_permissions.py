# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0035_layer_documentation'),
    ]

    operations = [
        migrations.AddField(
            model_name='groupmembership',
            name='can_edit_permissions',
            field=models.BooleanField(default=False),
        ),
    ]
