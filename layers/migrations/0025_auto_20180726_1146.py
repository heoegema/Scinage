# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('layers', '0024_cacheddata'),
    ]

    operations = [
        migrations.CreateModel(
            name='UploadedVideo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('video', models.FileField(upload_to=b'uploaded_videos')),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
        ),
        migrations.AlterField(
            model_name='cacheddata',
            name='timestamp',
            field=models.DateTimeField(auto_now=True),
        ),
        migrations.AlterField(
            model_name='control',
            name='input_type',
            field=models.IntegerField(db_column='input_type_id', choices=[(1, b'Text'), (2, b'Colorpicker'), (3, b'Image'), (4, b'Video'), (5, b'Textarea'), (6, b'Font (Google Fonts)')]),
        ),
    ]
