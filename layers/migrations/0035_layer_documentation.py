# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0034_auto_20190311_1253'),
    ]

    operations = [
        migrations.AddField(
            model_name='layer',
            name='documentation',
            field=models.TextField(blank=True),
        ),
    ]
