# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0023_slideshowslide_end_removal'),
    ]

    operations = [
        migrations.CreateModel(
            name='CachedData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField(max_length=500000)),
                ('timestamp', models.DateTimeField()),
                ('url', models.CharField(max_length=255)),
            ],
        ),
    ]
