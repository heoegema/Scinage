# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0020_auto_20170627_1346'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='channel',
            name='last_edit',
        ),
        migrations.RemoveField(
            model_name='channel',
            name='owner',
        ),
        migrations.RemoveField(
            model_name='channelitem',
            name='channel',
        ),
        migrations.RemoveField(
            model_name='channelitem',
            name='slide',
        ),
        migrations.RemoveField(
            model_name='channelitem',
            name='slideshow',
        ),
        migrations.RemoveField(
            model_name='contentgroup',
            name='channels',
        ),
        migrations.AddField(
            model_name='layer',
            name='changes',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='layer',
            name='publish',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='layer',
            name='trashed_at',
            field=models.DateTimeField(verbose_name='Trashed', null=True, editable=False, blank=True),
        ),
        migrations.AddField(
            model_name='slide',
            name='trashed_at',
            field=models.DateTimeField(verbose_name='Trashed', null=True, editable=False, blank=True),
        ),
        migrations.AddField(
            model_name='slideshow',
            name='trashed_at',
            field=models.DateTimeField(verbose_name='Trashed', null=True, editable=False, blank=True),
        ),
        migrations.AlterField(
            model_name='control',
            name='input_type',
            field=models.IntegerField(db_column='input_type_id', choices=[(1, b'Text'), (2, b'Colorpicker'), (3, b'Image'), (4, b'Textarea'), (5, b'Font (Google Fonts)')]),
        ),
        migrations.DeleteModel(
            name='Channel',
        ),
        migrations.DeleteModel(
            name='ChannelItem',
        ),
    ]