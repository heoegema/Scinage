# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('layers', '0012_auto_20151101_1552'),
    ]

    operations = [
        migrations.CreateModel(
            name='GroupMembership',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('can_add_users', models.BooleanField(default=False)),
                ('can_remove_users', models.BooleanField(default=False)),
                ('can_use_items', models.BooleanField(default=False)),
                ('can_add_items', models.BooleanField(default=False)),
                ('can_remove_items', models.BooleanField(default=False)),
                ('can_edit_items', models.BooleanField(default=False)),
            ],
        ),
        migrations.RenameField(
            model_name='channelitem',
            old_name='station',
            new_name='channel',
        ),
        migrations.RenameField(
            model_name='layer',
            old_name='creator',
            new_name='owner',
        ),
        migrations.RemoveField(
            model_name='contentgroup',
            name='owners',
        ),
        migrations.RemoveField(
            model_name='contentgroup',
            name='ro_users',
        ),
        migrations.RemoveField(
            model_name='contentgroup',
            name='rw_users',
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='owner',
            field=models.ManyToManyField(related_name='owned_groups', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='channel',
            name='last_edit',
            field=models.ForeignKey(related_name='channel_last_edit', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='groupmembership',
            name='contentgroup',
            field=models.ForeignKey(to='layers.ContentGroup', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='groupmembership',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='members',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, through='layers.GroupMembership'),
        ),
    ]
