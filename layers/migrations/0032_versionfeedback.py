# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('layers', '0031_auto_20190220_2009'),
    ]

    operations = [
        migrations.CreateModel(
            name='VersionFeedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('feedback_type', models.CharField(default=b'general', max_length=8, choices=[(b'bug', b'Bug/Issue'), (b'general', b'General Feedback'), (b'other', b'Other')])),
                ('feedback', models.TextField(blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
                ('version', models.ForeignKey(to='layers.Version', on_delete=models.CASCADE)),
            ],
        ),
    ]
