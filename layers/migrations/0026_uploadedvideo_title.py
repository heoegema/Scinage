# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0025_auto_20180726_1146'),
    ]

    operations = [
        migrations.AddField(
            model_name='uploadedvideo',
            name='title',
            field=models.CharField(max_length=255, blank=True),
        ),
    ]
