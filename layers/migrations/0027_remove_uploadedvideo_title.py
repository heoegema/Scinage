# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0026_uploadedvideo_title'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='uploadedvideo',
            name='title',
        ),
    ]
