# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0033_auto_20190311_1252'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='versionfeedback',
            options={'ordering': ['-creation_date']},
        ),
    ]
