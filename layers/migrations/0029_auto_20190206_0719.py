# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0028_auto_20190206_0635'),
    ]

    operations = [
        migrations.AlterField(
            model_name='slideshowslide',
            name='length',
            field=models.IntegerField(default=15),
        ),
    ]
