# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0022_auto_20180418_1439'),
    ]

    operations = [
        migrations.AddField(
            model_name='slideshowslide',
            name='end_removal',
            field=models.BooleanField(default=False),
        ),
    ]
