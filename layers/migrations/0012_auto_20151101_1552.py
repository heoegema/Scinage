# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('layers', '0011_auto_20151028_1303'),
    ]

    operations = [
        migrations.CreateModel(
            name='Channel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField(blank=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('edit_date', models.DateTimeField(auto_now=True, null=True)),
                ('last_edit', models.ForeignKey(related_name='station_last_edit', blank=True, to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)),
                ('owner', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='ChannelItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('priority', models.IntegerField(default=1)),
                ('start_date', models.DateTimeField()),
                ('end_date', models.DateTimeField()),
                ('repeat', models.CharField(max_length=200)),
                ('slide', models.ForeignKey(blank=True, to='layers.Slide', null=True, on_delete=models.CASCADE)),
                ('slideshow', models.ForeignKey(blank=True, to='layers.Slideshow', null=True, on_delete=models.CASCADE)),
                ('station', models.ForeignKey(to='layers.Channel', on_delete=models.CASCADE)),
            ],
        ),
        migrations.RemoveField(
            model_name='station',
            name='last_edit',
        ),
        migrations.RemoveField(
            model_name='station',
            name='owner',
        ),
        migrations.RemoveField(
            model_name='stationitem',
            name='slide',
        ),
        migrations.RemoveField(
            model_name='stationitem',
            name='slideshow',
        ),
        migrations.RemoveField(
            model_name='stationitem',
            name='station',
        ),
        migrations.RemoveField(
            model_name='contentgroup',
            name='owner',
        ),
        migrations.RemoveField(
            model_name='contentgroup',
            name='stations',
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='owners',
            field=models.ManyToManyField(related_name='owners', to=settings.AUTH_USER_MODEL),
        ),
        migrations.DeleteModel(
            name='Station',
        ),
        migrations.DeleteModel(
            name='StationItem',
        ),
        migrations.AddField(
            model_name='contentgroup',
            name='channels',
            field=models.ManyToManyField(to='layers.Channel', blank=True),
        ),
    ]
