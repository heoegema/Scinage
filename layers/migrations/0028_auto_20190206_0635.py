# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0027_remove_uploadedvideo_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='control',
            name='input_type',
            field=models.IntegerField(db_column='input_type_id', choices=[(1, b'Text'), (2, b'Colorpicker'), (3, b'Image'), (4, b'Textarea'), (5, b'Font (Google Fonts)'), (6, b'Video')]),
        ),
    ]
