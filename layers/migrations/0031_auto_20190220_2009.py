# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings
import layers.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('layers', '0030_auto_20190215_0738'),
    ]

    operations = [
        migrations.CreateModel(
            name='LayerRating',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rating', layers.models.IntegerRangeField()),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('edit_date', models.DateTimeField(auto_now=True, null=True)),
                ('layer', models.ForeignKey(to='layers.Layer', on_delete=models.CASCADE)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='layerrating',
            unique_together=set([('user', 'layer')]),
        ),
    ]
