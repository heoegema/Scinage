# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0021_auto_20180205_1605'),
    ]

    operations = [
        migrations.AddField(
            model_name='version',
            name='content_css',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='version',
            name='content_js',
            field=models.TextField(blank=True),
        ),
    ]
