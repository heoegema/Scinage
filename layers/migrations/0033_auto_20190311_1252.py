# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0032_versionfeedback'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='versionfeedback',
            options={'ordering': ['creation_date']},
        ),
    ]
