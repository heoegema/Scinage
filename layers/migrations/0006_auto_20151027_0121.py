# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0005_slideshow_slideshowslide'),
    ]

    operations = [
        migrations.RenameField(
            model_name='slideshowslide',
            old_name='order',
            new_name='s_order',
        ),
    ]
