# -*- coding: utf-8 -*-


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0019_auto_20160309_1946'),
    ]

    operations = [
        migrations.AddField(
            model_name='slideshowslide',
            name='time_end',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='slideshowslide',
            name='time_start',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='screen',
            name='title',
            field=models.CharField(help_text=b'If title changes after initial configuration, URL will change.', unique=True, max_length=255),
        ),
    ]
