# -*- coding: utf-8 -*-


from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Control',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('variable_name', models.CharField(max_length=18)),
                ('verbose', models.TextField(blank=True)),
                ('default', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='InputType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=255)),
                ('html', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Layer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('shared', models.BooleanField(default=False)),
                ('official', models.BooleanField(default=False)),
                ('title', models.CharField(max_length=255)),
                ('short', models.CharField(max_length=12)),
                ('description', models.TextField()),
                ('creator', models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE)),
            ],
        ),
        migrations.CreateModel(
            name='Version',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('content', models.TextField()),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
                ('changes', models.TextField(blank=True)),
                ('layer', models.ForeignKey(to='layers.Layer', on_delete=models.CASCADE)),
            ],
        ),
        migrations.AddField(
            model_name='control',
            name='input_type',
            field=models.ForeignKey(to='layers.InputType', on_delete=models.CASCADE),
        ),
        migrations.AddField(
            model_name='control',
            name='version',
            field=models.ForeignKey(to='layers.Version', on_delete=models.CASCADE),
        ),
    ]
