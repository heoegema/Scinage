Python Version: Python 3.7.3

Note: Please setup and run the virtual environment before doing anything else


## Virtual Environment
Setting up Virtual Env:
`pip3 install virtualenv`

`python3 -m venv Scinage`

Running Virtual Env:
`source bin/activate Scinage`

To exit virtual env:
`source deactivate`


## Setup:

`pip3 install -r requirements.txt`

`python3 manage.py migrate`


### Install poppler (this is needed for PDF to Slideshow generation) 

For Mac: 
`brew install poppler`

For Ubuntu 18
`sudo apt-get install poppler-utils`

For Heroku Deployment:
1. `heroku buildpacks:add --index 1 heroku-community/apt`
2. Create a file `Aptfile` which contains
`poppler-utils`
 
## Running:
`python3 manage.py runserver`

# Contributing

## Compiling SCSS/SASS files:
When scss/sass files are changed in `static/css` you must run:

```python3 manage.py compilescss```

to recompile the sass/scss files into css

## Setting up Custom Authentication

You have two options when it comes to using authentication:

1. Use a CAS (Central Authentication System)

2. Use our authentication system with optional social sign ins.

### Setting up CAS

If you choose to use a CAS, all you need to do is add a variable in

the file settings_local_example.py or settngs_local.py `CAS_SERVER_URL = '<enter link to CAS system here>`

Additonally, you will want to include the following in your settings:

```
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_cas_ng',
    ...
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django_cas_ng.middleware.CASMiddleware',
    ...
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'django_cas_ng.backends.CASBackend',
)

```

For more details around configuring your CAS system please refer to [the Official django_cas_ng documentation](https://pypi.org/project/django-cas-ng/).

## Setting up Custom Authentication

If you don't want to use a CAS system, there is already an authentication system set up. This will be used by default if no `CAS_SERVER_URL` value is found in settings.

Also be sure to set the following variables in the settings (in settings_local_example by default):

```
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = '<smtp.gmail.com | OR OTHER HOST>'
EMAIL_USE_TLS = True
EMAIL_PORT = 587
EMAIL_HOST_USER = '<EMAIL ADDRESS>'
EMAIL_HOST_PASSWORD = '<EMAIL PASSWORD>'
```


