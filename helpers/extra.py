import tweepy
import json
import requests
from django.conf import settings
from html.parser import HTMLParser
import re
from jinja2 import evalcontextfilter, Markup, escape
from layers.models import CachedData
from datetime import datetime, timedelta
from django.utils import timezone
from scinage.custom_logger import logger

# Get some tweets
def get_tweets(q, num=20, time_since=5):
    url = 'tweety' + str(q)
    update, obj = cacher(url, time_since)
    if update is False:
        data = obj.content
    else:
        auth = tweepy.OAuthHandler(*settings.TWITTER_KEYS)
        auth.set_access_token(*settings.TWITTER_TOKEN_KEYS)
        try:
            api = tweepy.API(auth)
            if '#' in q:
                search = api.search(q, rpp=num, tweet_mode='extended')
            elif '@' in q:
                search = api.user_timeline(screen_name=q.replace('@', ''), tweet_mode='extended')
            tweets = []
            for tweet in search:
                tweets.append(tweet._json)
            data = json.dumps(tweets)
        except Exception as e:
            logger.warning(e)
            data = "[]"
        if obj:
            obj.content = data
            obj.timestamp = timezone.now()
        else:
            obj = CachedData(content=data, url=url, timestamp=timezone.now())
        obj.save()
    return data


def get_request(url, time_since=15):
    update, obj = cacher(url, time_since)
    if update is False:
        data = obj.content
    else:
        try:
            data = json.dumps(requests.get(url).text)
        except Exception as e:
            data = "FAILED"
        if data.startswith('"'):
            data = data[1:-1]
        if obj:
            obj.content = data
            obj.timestamp = timezone.now()
        else:
            obj = CachedData(content=data, url=url, timestamp=timezone.now())
        obj.save()
    return data


def cacher(url, time_since=15):
    obj = CachedData.objects.filter(url=url).first()
    update = True
    if obj and timezone.now() - obj.timestamp < timedelta(minutes=time_since):
        update = False
    logger.info('update: %s, time_since: %s' % (update, time_since))
    return (update, obj)


def jquery(version='1.12.0'):
    return "<script src='/static/js/jquery-%s.min.js'></script>" % version


def banner_thief(url):
    class bannerParser(HTMLParser):
        def __init__(self):
            HTMLParser.__init__(self)
            self.recording = False
            self.data = []

        def handle_starttag(self, tag, attributes):
            if self.recording and tag == 'a':
                for name, value in attributes:
                    # If href is defined, grab it.
                    if name == "href":
                        splitlink = value.split('?')
                        self.data.append(
                            'https://uwaterloo.ca/%sajax/banner/?%s' %
                            (splitlink[0], splitlink[1])
                        )
            if tag == 'ul':
                for name, value in attributes:
                    if name == 'id' and value == 'uwb_paginator':
                        self.recording = True

        def handle_endtag(self, tag):
            if tag == 'ul' and self.recording:
                self.recording = False

    p = bannerParser()
    html = requests.get(url).text
    p.feed(html)

    string = ""
    for i in p.data:
        string += requests.get(i).text

    return string.replace('\n', '')


def linebreaks(value):
    """Converts newlines into <p> and <br />s."""
    value = re.sub(r'\r\n|\r|\n', '\n', value)  # normalize newlines
    paras = re.split('\n{2,}', value)
    paras = ['<p>%s</p>' % p.replace('\n', '<br />') for p in paras]
    paras = '\n\n'.join(paras)
    return Markup(paras)


def linebreaksbr(value):
    """Converts newlines into <p> and <br />s."""
    value = re.sub(r'\r\n|\r|\n', '\n', value)  # normalize newlines
    paras = re.split('\n{2,}', value)
    paras = ['%s' % p.replace('\n', '<br />') for p in paras]
    paras = '\n\n'.join(paras)
    return Markup(paras)


def font_replacer(value):
    return value.replace('+', ' ')


extra_globals = {
    'get_tweets': get_tweets,
    'get': get_request,
    'jquery': jquery,
    'banner_thief': banner_thief,
    'linebreaks': linebreaks,
    'linebreaksbr': linebreaksbr,
    'font_replacer': font_replacer,
}
