from django.contrib.auth.models import User
from scinage.custom_logger import logger


class UserMaskMiddleware(object):
    def process_request(self, request):

        if request.user.is_superuser and "__user_mask" in request.GET:
            request.session['__user_mask'] = request.GET["__user_mask"]
            logger.info(
                """request.user.username: %s,
                request.user.is_superuser: %s,
                request.session['__user_mask']: %s""" %
                (
                    request.user.username,
                    request.user.is_superuser,
                    request.session['__user_mask']
                )
            )
        elif "__user_unmask" in request.GET:
            del request.session['__user_mask']
        if request.user.is_superuser and '__user_mask' in request.session:
            mask = User.objects.filter(
                username=request.session['__user_mask']
            ).first()
            if mask is not None:
                request.user = mask
            else:
                del request.session['__user_mask']
