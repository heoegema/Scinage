from django.http import HttpResponse, JsonResponse
import json
from urllib.request import urlopen
from .custom_logger import logger


# YOU MUST HAVE A COPY OF THIS
def queryEmergency(request):
	'''
	This function should either return FALSE if there is no emergency, or an HttpResponse to display on the screen
	'''
	if checkForEmergency():
            # Go get the warning page
            try:
                r = urlopen('http://scicomp9-guest.uwaterloo.ca:8089/', timeout=0.5)
                html = r.read()
            except:
                logger.error("Could not reach notification, Falling back..")
                html = """
                 <h1>AN EMERGENCY HAS BEEN DETECTED</h1><p>The system could not reach the emergency details, please seek more information at DETAILS</p>
                """
            return HttpResponse(html)
	else:
		return False

def checkForEmergency():
    logger.info("CHECKING!")
    return False
    try:
        r = urlopen('http://scicomp9-guest.uwaterloo.ca:8089/emergency.json', timeout=0.5)
        data = json.loads(r.read())
        logger.info('data: %s' % data)
        return data.get('emergency', False)
    except Exception as e:
        logger.error("Could not get emergency info: %s" % str(e))
        return False
