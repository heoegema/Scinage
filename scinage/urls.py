"""scinage URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin, auth
import django_cas_ng.views
import haystack.urls
import layers
from layers import views as views
from layers import dashboard as dashboard
from rest_framework import routers
from layers.api import *

#Register api paths
router = routers.SimpleRouter()
router.register(r'layer', LayerAPIViewset, base_name='layer')

handler404 = layers.views.handler404

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^dashboard/', layers.dashboard.dashboard_all, name="dashboard"),
    url(r'^$', layers.views.frontpage, name="home"),
    url(r'^uploadimage/$', layers.views.ajaximage, name="upload_image"),
    url(r'^uploadvideo/$', layers.views.ajaxvideo, name="upload_video"),

    # Layer Editing
    url(r'^extracontrols/$', layers.views.controlsExtra, name="extra_controls"),
    url(r'^versionfeedback/(?P<version_id>\d+)/$', layers.views.versionFeedback, name="version_feedback"),

    url(r'^widgets/search/', include('haystack.urls')),
    url(r'^widgets/$', layers.views.manageLayers, name="layers"),
    url(r'^widgets/list/(?P<mode>\w+)/$', layers.views.listLayers, name="list_layers"),
    url(r'^widgets/new/$', layers.views.editLayernew, name="new_layer"),
    url(r'^widgets/clone/(?P<layer_id>\d+)/$', layers.views.cloneLayer, name="clone_layer"),
    url(r'^widgets/edit/(?P<layer_id>\d+)/$', layers.views.editLayernew, name="edit_layer"),
    url(r'^widgets/delete/(?P<layer_id>\d+)/$', layers.views.deleteLayer, name="delete_layer"),
    url(r'^widgets/view/(?P<layer_id>\d+)/$', layers.views.editLayer, name="view_layer"),
    url(r'^widgets/editorinput/(?P<id>\d+)/$', layers.views.loadInput),
    url(r'^widgets/favlayer/$', layers.views.toggleLayerFav, name='favlayer'),
    url(r'^widgets/offlayer/$', layers.views.toggleLayerOff, name='offlayer'),
    url(r'^widgets/ratelayer/$', layers.views.layerRating, name='ratelayer'),
    url(r'^widgets/documentation/(?P<version_id>\d+)/$', layers.views.layerDocumentation, name='documentation'),

    url(r'^widgets/newedit/$', layers.views.editLayernew, name="edit_layer_new_new"),
    url(r'^widgets/newedit/(?P<layer_id>\d+)/$', layers.views.editLayernew, name="edit_layer_new"),

    url(r'^widgets/changelog/$', layers.views.changeLog, name="layer_change_log"),

    # Slide Editing
    url(r'^slides/$', layers.views.manageSlides, name="slides"),
    url(r'^slides/list/(?P<mode>\w+)/$', layers.views.listSlides, name="list_slides"),
    url(r'^slides/new/$', layers.views.editSlide, name="new_slide"),
    url(r'^slides/clone/(?P<slide_id>\d+)/$', layers.views.cloneSlide, name="clone_slide"),
    url(r'^slides/edit/(?P<slide_id>\d+)/$', layers.views.editSlide, name="edit_slide"),
    url(r'^slides/delete/(?P<slide_id>\d+)/$', layers.views.deleteSlide, name="delete_slide"),
    url(r'^slides/layersettings/(?P<version_id>\d+)/$', layers.views.layerSettings),
    url(r'^slides/layerjson/new/(?P<version_id>\d+)/$', layers.views.newLayerJson),
    url(r'^slides/layerpreview/$', layers.views.previewLayer),

    # Slide Viewing
    url(r'^slides/view/(?P<slide_id>\d+)/$', layers.views.viewSlide, name="view_slide"),


    # Slideshow Viewing
    url(r'^slideshows/$', layers.views.manageSlideshows, name="slideshows"),
    url(r'^slideshows/list/(?P<mode>\w+)/$', layers.views.listSlideshows, name="list_slideshows"),
    url(r'^slideshows/pdf/upload/$', layers.views.uploadPDF, name="upload_pdf"),
    url(r'^slideshows/pdf/$', layers.views.createSlideshowPDF, name="create_slideshow_from_pdf"),
    url(r'^slideshows/new/$', layers.views.editSlideshow, name="new_slideshow"),
    url(r'^slideshows/clone/(?P<slideshow_id>\d+)/$', layers.views.cloneSlideshow, name="clone_slideshow"),
    url(r'^slideshows/edit/(?P<slideshow_id>\d+)/$', layers.views.editSlideshow, name="edit_slideshow"),
    url(r'^slideshows/delete/(?P<slideshow_id>\d+)/$', layers.views.deleteSlideshow, name="delete_slideshow"),
    url(r'^slideshows/nextslide/(?P<slideshow_id>\d+)/(?P<current_index>\d+)/$', layers.views.nextSlide),
    url(r'^slideshows/view/(?P<slideshow_id>\d+)/$', layers.views.viewSlideshow, name="view_slideshow"),


    # Screen Viewing
    url(r'^screens/$', layers.views.manageScreens, name="screens"),
    url(r'^screens/list/$', layers.views.listScreens, name="list_screens"),
    url(r'^screens/new/$', layers.views.editScreen, name="new_screen"),
    url(r'^screens/edit/(?P<screen_id>\d+)/$', layers.views.editScreen, name="edit_screen"),
    url(r'^screens/delete/(?P<screen_id>\d+)/$', layers.views.editScreen, name="delete_screen"),
    url(r'^screens/view/(?P<slug>[-\w\d]+)/$', layers.views.viewScreen, name="view_screen"),
    url(r'^screens/view/(?P<slug>\d]+)/$', layers.views.viewScreen, name='view_screen_id'),
    url(r'^screens/ping/(?P<slug>[-\w\d]+)/$', layers.views.pingScreen, name="ping_screen"),



    # Group stuff
    url(r'^groups/$', layers.views.manageGroups, name="groups"),
    url(r'^groups/list/(?P<mode>\w+)/$', layers.views.listGroups, name="list_groups"),
    url(r'^groups/new/$', layers.views.editGroup, name="new_group"),
    url(r'^groups/edit/(?P<group_id>\d+)/$', layers.views.editGroup, name="edit_group"),
    url(r'^groups/view/(?P<group_id>\d+)/$', layers.views.viewGroup, name="view_group"),
    url(r'^groups/delete/(?P<group_id>\d+)/$', layers.views.editGroup, name="delete_group"),

    #Images
    url(r'^images/$', layers.views.images, name="images"),
    url(r'^image-upload/$', layers.views.imageUpload, name="image_upload"),
    url(r'^images/delete/(?P<id>[0-9]+)/$', layers.views.deleteImage, name="image_delete"),

    #Videos
    url(r'^videos/$', layers.views.videos, name="videos"),
    url(r'^video-upload/$', layers.views.videoUpload, name="video_upload"),
    url(r'^videos/delete/(?P<id>[0-9]+)/$', layers.views.deleteVideo, name="video_delete"),

    # Sharing
    url(r'^share/(?P<item>\w+)/(?P<pk>\d+)/$', layers.views.shareSettings, name="sharesettings"),
    url(r'^transfer/(?P<item>\w+)/(?P<pk>\d+)/$', layers.views.transferOwnership, name="transfer"),

    #trashbin
    url(r'^trashbin/$', layers.views.trashbin, name='trashbin'),
    url(r'^trashbin/delete/(?P<model>\w+)/(?P<id>[0-9]+)/$', layers.views.delete, name='delete'),
    url(r'^trashbin/restore/(?P<model>\w+)/(?P<id>[0-9]+)/$', layers.views.restore, name='restore'),

    #API routes
    # url(r'^api/', include(router.urls, namespace="api")),

]

from django.conf import settings

# Handling authentication routes

# We want to have the option for a CAS (Central Authentication System) to be configured

# We check for the prescene of a CAS_SERVER_URL in settings
# If it is present we will authenticate through that CAS Server
# Likewise, if it's not then we will use our own authentication system
if settings.CAS_SERVER_URL is not None:
    urlpatterns.extend([
        url(r'^accounts/login/$', django_cas_ng.views.LoginView.as_view(), name='login'),
        url(r'^accounts/logout/$', django_cas_ng.views.LogoutView.as_view(), name='logout')
    ])
else:
    urlpatterns.extend([
        url(r'^accounts/signup/$', layers.views.SignUp.as_view(), name='signup'),
        url('^', include('django.contrib.auth.urls')),
        # url(r'^accounts/password_reset/$', layers.views.PasswordReset.as_view(), name='password_reset'),
        url(r'^accounts/', include('django.contrib.auth.urls')),
    ])

from django.conf.urls.static import static

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static('/media/', document_root=settings.MEDIA_ROOT)
