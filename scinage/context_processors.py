from django.conf import settings

# Small context processor...
def newlysaved(request):
    if request.session.pop('newlysaved', False):
        return {'newlysaved': True}
    else:
        return {}

def doc_url(request):
	return {'doc_url': getattr(settings, 'DOC_URL', '/')}
